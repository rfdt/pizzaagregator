import axios from 'axios';

export const OrdersApi = {
    createOrder(newOrder, config){
        return axios.post('/api/orders/create', newOrder, config);
    },
    getOrders(config){
        return axios.get('/api/orders/', config);
    },
    rateOrders(id, rate, config){
        return axios.post('/api/orders/rate', {id, rate}, config);
    },
    activeCoupon(couponname,config){
        return axios.get(`/api/coupon/${couponname}`, config);
    },
    getSomeOrders(stage, config){
        return axios.get(`/api/orders/${stage}`, config);
    },
    pickCookerOrder(id, cookId, cookName, config){
        return axios.post(`/api/orders/pickcookerorder`, {id, cookId, cookName}, config);
    },
    pickDelivOrder(id, deliverymanId, deliverymanName, deliverymanPhone, config){
        return axios.post(`/api/orders/pickdelivorder`, {id, deliverymanId, deliverymanName, deliverymanPhone}, config);
    },
    getCookerOrder(id, config){
        return axios.get(`/api/orders/getcookerorder/${id}`,  config);
    },
    finishCook(id, config){
        return axios.post(`/api/orders/finishcookorder`, {id}, config);
    },
    finishDelivery(id, config){
        return axios.post(`/api/orders/finishdeliveryorder`, {id}, config);
    }
}