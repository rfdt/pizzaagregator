import axios from 'axios';

export const ProductsApi = {
    loadProducts(config){
        return axios.get('/api/products', config)
    },
    loadHotProducts(config){
        return  axios.get('/api/products/hot', config)
    }
}