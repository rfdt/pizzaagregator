import axios from 'axios';

export const UserApi = {
    loadUser(token){
        return axios.get('/api/auth/user', token);
    },
    loginUser(body, config){
        return axios.post('/api/auth/login', body, config);
    },
    checkoutUser(body, config){
        return  axios.post('/api/auth/checkout', body, config);
    },
    registerUser(body, config){
        return  axios.post('/api/auth/register', body, config);
    },
    addToCart(product, config){
        return axios.post('/api/user/addtocart', product, config);
    },
    romoveFromCart(editedCart, config){
        return axios.post('/api/user/removefromcart', editedCart, config);
    },
    addCv(body){
        return axios.post('/api/cv/create', body);
    },
    addNewProduct(newProduct, config){
        return axios.post('/api/products/create', newProduct, config);
    },
    getEveryDayPlane(config){
        return axios.get('/api/admin/getdayplane', config);
    },
    getMonthPlane(config){
        return axios.get('/api/admin/getmonthplane', config);
    },
    getTodayOrders(config){
        return axios.get('/api/admin/gettodayorders', config);
    },
    getYestardayOrders(config){
        return axios.get('/api/admin/getyestardayorders', config);
    },
    getMonthOrders(config){
        return axios.get('/api/admin/getmonthorders', config);
    },
    updateProduct(product, config){
        return axios.post(`/api/products/update/${product._id}`, product, config);
    },
    updateProductPhoto(product, currentPickedPhoto, photoFile, config){
      return axios.post(`/api/products/updatephoto/${product.type}/${product._id}/${currentPickedPhoto}`, photoFile, config);
    },
    updateCarouselPhoto(type, photoFile, config){
        return axios.post(`/api/admin/updatehotphoto/${type}`, photoFile, config);
    },
    viewCvs(){
       return axios.get(`/api/cv/`);
    },
    markedCvs(id){
        return axios.get(`/api/cv/marked/${id}`);
    },
    deleteCvs(id){
        return axios.get(`/api/cv/delete/${id}`);
    },
    activeUser(body, config){
        return axios.post('/api/auth/verify', body, config);
    },
    getAllUsers(config){
        return axios.get('/api/admin/getallusers', config);
    },
    changeUserType(userId, userType, config){
        return axios.get(`/api/admin/changeusertype/${userId}/${userType}`, config)
    },
    createCoupon(coupon, config){
        return axios.post('/api/coupon/create', coupon, config);
    }
};
