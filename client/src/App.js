import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {Redirect, Route, Switch} from "react-router";
import {asyncLoadUserAC} from "./Store/User/userActions";
import {loadProductsThunk} from "./Store/Products/productsActions";
import {loadHotProductsThunk} from "./Store/Hot/hotActions";
import Login from "./Modules/LoginModule/Login";
import Main from "./Modules/MainModule/Main";
import Menu from "./Modules/Header/Menu";
import Footer from "./Modules/Footer/Footer";
import CartModule from "./Modules/CartModule/CartModule";
import Register from "./Modules/RegisterModule/Register";
import OrdersModule from "./Modules/OrdersModule/OrdersModule";
import Cv from "./Modules/CvModule/Cv";
import CookerMainPageLazy from "./Modules/CookerModule/LazyModules/CookerMainPageLazy";
import CookerOrderLazy from "./Modules/CookerModule/LazyModules/CookerOrderLazy";
import AdminMainPageLazy from "./Modules/AdminModule/AdminMainPageLazy/AdminMainPageLazy";
import DeliveryOrderLazy from "./Modules/DeliveryModule/LazyModules/DeliveryOrderLazy";
import DeliveryMainPageLazy from "./Modules/DeliveryModule/LazyModules/DeliveryMainPageLazy";
import VerifyModule from "./Modules/VerifyModule/VerifyModule";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'antd/dist/antd.css'
import './App.css';


function App(props) {

    useEffect(()=>{
        // props.loadUserThunk();
        props.asyncLoadUserAC();
        props.loadProductsThunk();
        props.loadHotProductsThunk();
        // eslint-disable-next-line
    }, []);

    return (
        <div className="App">
            <Menu/>
            <div className='App-Route-Container'>
                <Switch>
                    <Route exact path={'/orders'}>
                        {props.user.isAuthenticated ? <OrdersModule /> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/cart'}>
                        {props.user.isAuthenticated ? <CartModule /> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/login'}>
                        {!props.user.isAuthenticated ? <Login/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/register'}>
                        {!props.user.isAuthenticated ? <Register/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/verify/:id'}>
                        {!props.user.isAuthenticated ? <VerifyModule/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/cooker/:id'}>
                        {props.user.isAuthenticated && props.user.user.userType === 'cooker' ? <CookerOrderLazy/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/cooker'}>
                        {props.user.isAuthenticated && props.user.user.userType === 'cooker' ? <CookerMainPageLazy/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/deliv/:id'}>
                        {props.user.isAuthenticated && props.user.user.userType === 'delivery' ? <DeliveryOrderLazy/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/deliv'}>
                        {props.user.isAuthenticated && props.user.user.userType === 'delivery' ? <DeliveryMainPageLazy/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/admin'}>
                        {props.user.isAuthenticated && props.user.user.userType === 'admin' ? <AdminMainPageLazy/> : <Redirect to='/' />}
                    </Route>
                    <Route exact path={'/work'}>
                       <Cv />
                    </Route>
                    <Route path={'/'} component={Main}/>
                </Switch>
            </div>
            <Footer/>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, { loadProductsThunk, loadHotProductsThunk, asyncLoadUserAC})(App);

//loadUserThunk