import {AutoComplete, Button, Form, Input, InputNumber, message, Modal, Select} from "antd";
import React, { useState} from "react";
import {formItemLayout} from "../../CvModule/Cv";
import {SmileOutlined} from "@ant-design/icons";
import {connect} from "react-redux";
import {createCoupon} from "../../../Store/User/adminActions";

const {Option} = AutoComplete;

const AddNewCoupon = (props) => {

    const [visible, setVisible] = useState(false);
    const [couponType, setCouponType] = useState('perc');
    const [couponName, setCouponName] = useState('');
    const [minSum, setMinSum] = useState(1000);
    const [perc, setPerc] = useState(0.01);
    const [sum, setSum] = useState(50);
    const [gift, setGift] = useState({
        type : "",
        name : "",
        cost : 0,
        size : "small",
        discount : 1,
        toppings : []
    });

    const showModal = () => {
        setVisible(true);
    };

    const handleOk = () => {
        setVisible(false);
    };

    const handleCancel = () => {
        setVisible(false);
    };

    const submitCoupon = () =>{
        const coupon = {};
        if (!couponType || !couponName || !minSum) return  message.error('Введите все поля');
        coupon['name'] = couponName;
        coupon['type'] = couponType;
        coupon['minsum'] = minSum;
        if (couponType === 'perc'){
            if(!perc) return message.error('Введите процент скидки');
            coupon['perc'] = perc
        }
        if (couponType === 'sum'){
            if(!sum) return message.error('Введите сумму скидки');
            coupon['sum'] = sum
        }
        if (couponType === 'gift'){
            if(!gift.name || !gift.type) return message.error('Выберите подарочное блюдо');
            coupon['gift'] = gift
        }
        props.createCoupon(coupon).then(res=>{
            message.success(`Купон ${res.data.name} создан`)
        }).catch(err=>{
            message.error(`Ошибка: ${err.response.msg}`)
        })
    };


    return (
        <>
            <Button type="primary" onClick={showModal} style={{width: '50%', marginBottom: '15px'}} className='Admin__Button'>
                Добавить купон
            </Button>
            <Modal
                title="Изменение пользователя"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
                    <Form.Item
                        {...formItemLayout}
                        label="Тип купона: "
                        className='loginforminputs'
                    >
                        <Select defaultValue={couponType} value={couponType}
                                onChange={(value) => {
                                   setCouponType(value);
                                }}>
                            <Select.Option value="perc">Процентный</Select.Option>
                            <Select.Option value="sum">Сумма</Select.Option>
                            <Select.Option value="gift">Подарок</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="Имя купона"
                        className='loginforminputs'
                    >
                        <Input placeholder="Введите имя купона" value={couponName}
                               onChange={(event) => {
                                   setCouponName(event.target.value);
                               }}
                               name='couponName' prefix={<SmileOutlined/>}/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        label="Мин.сумма заказа: "
                        className='loginforminputs'
                    >
                        <InputNumber size="large" min={1} max={100000} defaultValue={minSum} value={minSum} onChange={(value)=>{
                            setMinSum(value);
                        }} />
                    </Form.Item>
                {
                    couponType === "perc" && <Form.Item
                        {...formItemLayout}
                        label="Процент скидки: "
                        className='loginforminputs'
                    >
                        <InputNumber size="large" min={0.01} max={100} defaultValue={perc} value={perc} step={0.01} formatter={value => `${value}%`}onChange={(value)=>{
                            setPerc(value);
                        }} />
                    </Form.Item>
                }
                {
                    couponType === "sum" && <Form.Item
                        {...formItemLayout}
                        label="Сумма скидки: "
                        className='loginforminputs'
                    >
                        <InputNumber size="large" formatter={value => `${value}₽`} min={1} max={100000} defaultValue={sum} value={sum} step={10} onChange={(value)=>{
                            setSum(value);
                        }} />
                    </Form.Item>
                }
                {
                    couponType === "gift" && <Form.Item
                        {...formItemLayout}
                        label="Подарочный товар: "
                        className='loginforminputs'
                    >
                        <Select style={{width: '100%'}} value={gift.type + ' ' + gift.name}
                                onChange={(value)=>{
                                    let finishValue = value.split('XXX');
                                    console.log(finishValue);
                                    setGift(prevState => {
                                        return {
                                            ...prevState,
                                            type: finishValue[0],
                                            name: finishValue[1]
                                        }
                                    })
                                }}>
                            {props.products.products.map((product) => (
                                <Option key={product._id} value={product.type + 'XXX' + product.name}>{product.name}</Option>
                            ))}
                        </Select>
                    </Form.Item>
                }
                <Form.Item
                    {...formItemLayout}
                    label="Создать "
                    className='loginforminputs'
                >
                    <Button type={'primary'} onClick={submitCoupon}>Создать купон</Button>
                </Form.Item>
            </Modal>
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps, {createCoupon})(AddNewCoupon);