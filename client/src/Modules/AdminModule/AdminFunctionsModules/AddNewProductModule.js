import React, {useState} from 'react';
import {Modal, Button, message, Form, Input, Select, InputNumber} from 'antd';
import {connect} from "react-redux";
import * as Yup from "yup";
import {useFormik} from "formik";
import {addNewProduct} from "../../../Store/User/adminActions";
import {SmileOutlined} from "@ant-design/icons";
import {formItemLayout} from "../../CvModule/Cv";
import '../AdminModule.css'


const newProductSchema = Yup.object().shape({
    type: Yup.string().required('Выберите тип продукта'),
    name: Yup.string().required('Введите название позиции'),
    desk: Yup.string().required('Введите полное описание позиции'),
    shortdesk: Yup.string().required('Введите полное описание позиции'),
    cost: Yup.number().required('Введите цену позиции'),
    discount: Yup.number().required('Введите скидку в формате 1 - нет скидки, 0.9 - 10 процентов скидки'),
    hot: Yup.boolean()
});

const {TextArea} = Input;

const AddNewProductModule = (props) => {

    const formik = useFormik({
        initialValues: {
            type: 'pizza',
            name: '',
            desk: '',
            shortdesk: '',
            cost: 0,
            discount: 1,
            hot: false
        },
        validationSchema: newProductSchema,
        onSubmit: (values, {resetForm}) => {
            let newProduct = {};
            Object.assign(newProduct, values);
            props.addNewProduct(newProduct).then(res=>{
                message.success(`Позиция добавлена успешно`);
                message.success(`Id новой позиции ${res.data._id}`);
                resetForm(formik.initialValues);
                handleCancel();
            }).catch((err)=>{
                message.error('Проблема с добавлением позиции')
            })
        },
    });

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" style={{width: '50%'}} onClick={showModal} className='Admin__Button'>
                Добавить продукт
            </Button>
            <Modal
                title="Добавление продукта"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
                    <form onSubmit={formik.handleSubmit}>
                        <Form.Item
                            {...formItemLayout}
                            label="Тип позиции"
                            className='loginforminputs'
                            validateStatus={formik.errors.type && formik.touched.type && 'error'}
                            help={formik.touched.type && formik.errors.type}
                        >
                            <Select defaultValue={formik.values.type} value={formik.values.type} onChange={(value) => {
                                formik.setFieldValue('type', value)
                            }}>
                                <Select.Option value="pizza">Пицца</Select.Option>
                                <Select.Option value="rolls">Роллы</Select.Option>
                                <Select.Option value="frrolls">Запеченные роллы</Select.Option>
                                <Select.Option value="temprolls">Темпура роллы</Select.Option>
                                <Select.Option value="wok">WOK Лапша</Select.Option>
                                <Select.Option value="desserts">Дессерты</Select.Option>
                                <Select.Option value="drinks">Напитики</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Имя позиции"
                            className='loginforminputs'
                            validateStatus={formik.errors.name && formik.touched.name && 'error'}
                            help={formik.touched.name && formik.errors.name}
                        >
                            <Input placeholder="Введите имя" value={formik.values.name}
                                   onChange={formik.handleChange}
                                   name='name' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Полное описание"
                            className='loginforminputs'
                            validateStatus={formik.errors.desk && formik.touched.desk && 'error'}
                            help={formik.touched.desk && formik.errors.desk}
                        >
                            <TextArea placeholder="Введите описание" value={formik.values.desk}
                                   onChange={formik.handleChange}
                                   name='desk' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Краткое описание"
                            className='loginforminputs'
                            validateStatus={formik.errors.shortdesk && formik.touched.shortdesk && 'error'}
                            help={formik.touched.shortdesk && formik.errors.shortdesk}
                        >
                            <TextArea placeholder="Введите краткое описание" value={formik.values.shortdesk}
                                   onChange={formik.handleChange}
                                   name='shortdesk' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Цена"
                            className='loginforminputs'
                            validateStatus={formik.errors.cost && formik.touched.cost && 'error'}
                            help={formik.touched.cost && formik.errors.cost}
                        >
                            <InputNumber value={formik.values.cost} style={{width: '100%'}} defaultValue={formik.values.cost} onChange={(value) => {
                                formik.setFieldValue('cost', value)
                            }}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Скидка"
                            className='loginforminputs'
                            validateStatus={formik.errors.discount && formik.touched.discount && 'error'}
                            help={formik.touched.discount && formik.errors.discount}
                        >
                            <InputNumber value={formik.values.discount} min={0} max={1} step={0.1} style={{width: '100%'}} defaultValue={formik.values.discount} onChange={(value) => {
                                formik.setFieldValue('discount', value)
                            }}/>
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="Горячий продукт"
                            className='loginforminputs'
                            validateStatus={formik.errors.hot && formik.touched.hot && 'error'}
                            help={formik.touched.hot && formik.errors.hot}
                        >
                            <Select defaultValue={formik.values.hot} value={formik.values.hot} onChange={(value) => {
                                formik.setFieldValue('hot', value)
                            }}>
                                <Select.Option value={true}>Добавить</Select.Option>
                                <Select.Option value={false}>Не добавлять</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item>
                            <div className='Cv__Submit-Button'>
                                <Button type="primary" htmlType="submit" style={{width: "180px", textAlign: 'center'}}
                                        className='submit-button'>
                                    Добавить товар
                                </Button>
                            </div>
                        </Form.Item>
                    </form>
            </Modal>
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, {addNewProduct})(AddNewProductModule);