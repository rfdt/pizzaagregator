import React, {useEffect, useState} from 'react';
import '../AdminModule.css';
import {connect} from "react-redux";
import {
    getEveryDayPlane,
    getMonthOrders,
    getMonthPlane,
    getTodayOrders,
    getYestardayOrders
} from "../../../Store/User/adminActions";
import {Progress, Rate} from "antd";

const AdminStatsModule = (props) => {

    const [everyDayPlane, setEveryDayPlane] = useState(0);
    const [monthPlane, setMonthPlane] = useState(0);
    const [todayOrder, setTodayOrders] = useState([]);
    const [yesterdayOrders, setYesterdayOrders] = useState([]);
    const [monthOrders, setMonthOrders] = useState([]);
    const [rate, setRate] = useState(5);
    const [totalValueOrders, setTotalValueOrders] = useState(1);

    useEffect(() => {
        let totalRate = 0;
        let totalValue = 0;
        monthOrders.forEach((item)=>{
            if(item.stage === 5){
                totalRate += item.rate;
                totalValue += 1;
            }
        });

        setRate(totalRate);
        setTotalValueOrders(totalValue);
        // eslint-disable-next-line
    }, [monthOrders.length]);

    useEffect(()=>{
        props.getEveryDayPlane().then(res=>{
           setEveryDayPlane(res.data['dayPlane'].value);
        });
        props.getMonthPlane().then((res)=>{
            setMonthPlane(res.data['monthPlane'].value);
        });
        props.getTodayOrders().then(res=>{
            setTodayOrders(res.data.todayOrder);
        });
        props.getYestardayOrders().then(res=>{
            setYesterdayOrders(res.data.yestardayOrder)
        });
        props.getMonthOrders().then(res=>{
            setMonthOrders(res.data.monthOrders)
        })
        // eslint-disable-next-line
    }, []);

    return (
        <div className='Stats__Container'>
            <div className="Stats__Title">
                Статистика
            </div>
            <div className="Stats__TodayOrders">
                <div className="TodayOrders__Title">Ежемесяный план: {monthPlane} заказов.</div>
                <div className="TodayOrders__Title">Ежедневный план: {everyDayPlane} заказов.</div>
                <div className="TodayOrders__Title">Статистика сегодня</div>
                <div className="Stats__Module">
                    <div className='Stats__Module-Container'>
                        <div className="Stats__Title">Дневной план</div>
                        <Progress type="circle" trailColor='#bfbfbf'
                                  percent={(todayOrder.length / everyDayPlane) * 100}
                                  format={percent => `${todayOrder.length}  из ${everyDayPlane} заказов`} />
                    </div>
                    <div className='Stats__Module-Container'>
                        <div className="Stats__Title">Сравнение с вчера</div>
                        <Progress type="circle" trailColor='#bfbfbf'
                                  percent={(todayOrder.length / yesterdayOrders.length) * 100}
                                  format={percent => `${todayOrder.length} из ${yesterdayOrders.length}`} />
                    </div>
                </div>
                <div className="TodayOrders__Title">Месячный план</div>
                <div className="Stats__Module-Month">
                    <div className='Stats__Module-Container'>
                        <div className="Stats__Title">Ежемесячный план</div>
                        <Progress type="circle" trailColor='#bfbfbf'
                                  percent={(monthOrders.length / monthPlane) * 100}
                                  format={percent => `${monthOrders.length}  из ${monthPlane} заказов`} />
                    </div>
                    <div className='Stats__Module-Container'>
                        <div className="Stats__Title">Средняя оценка за месяц</div>
                        {totalValueOrders > 0 ? <Rate disabled allowHalf  value={rate/totalValueOrders}/> : <div>Нет оконченных заказов</div>}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default connect(null, {getEveryDayPlane, getTodayOrders, getYestardayOrders, getMonthPlane, getMonthOrders})(AdminStatsModule);