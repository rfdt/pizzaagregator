import React, {useState} from 'react';
import {Button, Form, Image, message, Modal, Select, Tooltip, Upload} from "antd";
import {formItemLayout} from "../../CvModule/Cv";
import {UploadOutlined} from "@ant-design/icons";
import {connect} from "react-redux";
import {updateCarouselPhoto} from "../../../Store/User/adminActions";

const {Option} = Select;
const {Item} = Form;

const ChangeCarousel = (props) => {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [currentPickedPhoto, setCurrentPickedPhoto] = useState('1');
    const [file, setFile] = useState([]);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const submitPhotoHandler = ({fileList}) => {
        setFile(prevState => fileList);
    };

    const uploadPhotoCarousel = () =>{
        if (file.length === 0) return message.warning('Выберите файл');
        let photoFile = new FormData();
        photoFile.append("photo", file[0].originFileObj);

        props.updateCarouselPhoto(currentPickedPhoto, photoFile).then(res=>{
            message.success(`Фото ${res.data.name} обновлено`);
            setFile([]);
            setCurrentPickedPhoto('1')
        }).catch(err=>{
            message.error(`Ошибка обновления`);
            setFile([]);
            setCurrentPickedPhoto('1')
        })
    };

    return (
        <>
            <Button type="primary" onClick={showModal} className='Admin__Button'>
                Изменить акции
            </Button>
            <Modal
                title="Изменение фото акций"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
                <Item
                    {...formItemLayout}
                    label="Фото карусели"
                    className='loginforminputs'
                    key={'photos'}
                >
                    <Image.PreviewGroup>
                        <Image
                            src={`/images/hot/carousel/car_1.jpg`}
                            alt={'Нет фото 1 превью'}
                        />
                        <Image
                            src={`/images/hot/carousel/car_2.jpg`}
                            alt={'Нет фото 2 превью'}
                        />
                        <Image
                            src={`/images/hot/carousel/car_3.jpg`}
                            alt={'Нет фото 3 превью'}
                        />
                    </Image.PreviewGroup>
                </Item>
                <Item
                    {...formItemLayout}
                    label="Изменить фото"
                    className='loginforminputs'
                    key={'photos'}
                >
                    <h1>Фотографии должны быть</h1>
                    <h2>В разрешении 1280*720</h2>
                    <h2>В формате .jpg или .jpeg</h2>
                    <Select defaultValue={currentPickedPhoto} value={currentPickedPhoto} onChange={(value) => {
                        setCurrentPickedPhoto(value);
                    }}>
                        <Option value="1">1 Фото</Option>
                        <Option value="2">2 Фото</Option>
                        <Option value="3">3 Фото</Option>
                    </Select>
                    <div style={{
                        marginTop: '10px',
                    }}>
                        <Upload
                            listType="picture-card"
                            fileList={file}
                            onChange={submitPhotoHandler}
                            beforeUpload={() => false}
                            style={{
                                display: 'flex',
                                justifyContent: 'center'
                            }}
                        >
                            <div>
                                <UploadOutlined/>
                                <div className="ant-upload-text">Вставить</div>
                            </div>
                        </Upload>
                        <Tooltip title="Нажмите чтобы загрузить">
                            <Button type="primary" icon={<UploadOutlined/>} style={{width: '100%'}}
                                    onClick={uploadPhotoCarousel}/>
                        </Tooltip>
                    </div>
                </Item>
            </Modal>
        </>
    );
};

export default connect(null, {updateCarouselPhoto})(ChangeCarousel);