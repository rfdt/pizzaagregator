import React, {useState} from 'react';
import {Button, Form, Input, InputNumber, message, Modal, Select, Image, Upload, Tooltip} from "antd";
import {UploadOutlined} from '@ant-design/icons';
import {formItemLayout} from "../../CvModule/Cv";
import {SmileOutlined} from "@ant-design/icons";
import {connect} from "react-redux";
import '../AdminModule.css';
import {updateProduct, updateProductPhoto} from "../../../Store/User/adminActions";

const {Option} = Select;
const {Item} = Form;
const {TextArea} = Input;

const ChangeProduct = (props) => {

    const uploadButton = (
        <div>
            <UploadOutlined/>
            <div className="ant-upload-text">Вставить</div>
        </div>
    );

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isPickedProduct, setIsPickedProduct] = useState(false);
    const [currentPickedProduct, setCurrentPickedProduct] = useState({});
    const [file, setFile] = useState([]);
    const [currentPickedPhoto, setCurrentPickedPhoto] = useState('preview');

    const submitHandler = () => {
        props.updateProduct(currentPickedProduct).then(res => {
            message.success(`Позиция ${res.data.name} обновлена`);
            setIsPickedProduct(false);
            setCurrentPickedProduct({});
        });
    };

    const uploadPhoto = () =>{
        if (file.length === 0) return message.warning('Выберите файл');
        let photoFile = new FormData();
        photoFile.append("photo", file[0].originFileObj);

        props.updateProductPhoto(currentPickedProduct, currentPickedPhoto, photoFile).then(res=>{
            message.success(`Фото ${res.data.name} обновлено`);
            setFile([]);
            setCurrentPickedPhoto('preview')
        }).catch(err=>{
            message.error(`Ошибка обновления`);
            setFile([]);
            setCurrentPickedPhoto('preview')
        })
    };

    const submitPhotoHandler = ({fileList}) => {
        setFile(prevState => fileList);
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const selectHandler = (value) => {
        setIsPickedProduct(true);
        setCurrentPickedProduct(props.products.products.filter((product) => product._id === value)[0])
    };

    return (
        <>
            <Button type="primary" style={{width: '50%'}} onClick={showModal} className='Admin__Button'>
                Изменить продукт
            </Button>
            <Modal
                title="Изменение продукта"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
                <div className="ChangeProduct__Title">Выберите продукт</div>
                <Select disabled={isPickedProduct} style={{width: '100%'}} value={currentPickedProduct._id}
                        onChange={selectHandler}>
                    {props.products.products.map((product) => (
                        <Option key={product._id} value={product._id}>{product.name}</Option>
                    ))}
                </Select>
                {isPickedProduct && <div className="ChangeProduct__Product">
                    <div className="ChangeProduct__Name">Текущий продукт</div>
                    <div className="ChangeProduct__Name">{currentPickedProduct.name}</div>
                    <div className="ChangeProduct__Name">{currentPickedProduct._id}</div>
                    <div className='ChangeProduct__Form'>
                        <Item
                            {...formItemLayout}
                            label="Тип позиции"
                            className='loginforminputs'
                            key={'type'}
                        >
                            <Select defaultValue={currentPickedProduct.type} value={currentPickedProduct.type}
                                    onChange={(value) => {
                                        setCurrentPickedProduct(prevState => ({
                                            ...prevState,
                                            type: value
                                        }))
                                    }}>
                                <Select.Option value="pizza">Пицца</Select.Option>
                                <Select.Option value="rolls">Роллы</Select.Option>
                                <Select.Option value="frrolls">Запеченные роллы</Select.Option>
                                <Select.Option value="temprolls">Темпура роллы</Select.Option>
                                <Select.Option value="wok">WOK Лапша</Select.Option>
                                <Select.Option value="desserts">Дессерты</Select.Option>
                                <Select.Option value="drinks">Напитики</Select.Option>
                            </Select>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Имя позиции"
                            className='loginforminputs'
                            key={'name'}
                        >
                            <Input placeholder="Введите имя" value={currentPickedProduct.name}
                                   onChange={(event) => {
                                       setCurrentPickedProduct(prevState => ({
                                           ...prevState,
                                           name: event.target.value
                                       }))
                                   }}
                                   name='name' prefix={<SmileOutlined/>}/>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Полное описание"
                            className='loginforminputs'
                            key={'desk'}
                        >
                            <TextArea placeholder="Введите описание" value={currentPickedProduct.desk}
                                      onChange={(event) => {
                                          setCurrentPickedProduct(prevState => ({
                                              ...prevState,
                                              desk: event.target.value
                                          }))
                                      }}
                                      name='desk' prefix={<SmileOutlined/>}/>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Краткое описание"
                            className='loginforminputs'
                            key={'shortdesk'}
                        >
                            <TextArea placeholder="Введите краткое описание" value={currentPickedProduct.shortdesk}
                                      onChange={(event) => {
                                          setCurrentPickedProduct(prevState => ({
                                              ...prevState,
                                              shortdesk: event.target.value
                                          }))
                                      }}
                                      name='shortdesk' prefix={<SmileOutlined/>}/>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Цена"
                            className='loginforminputs'
                            key={'cost'}
                        >
                            <InputNumber value={currentPickedProduct.cost} style={{width: '100%'}}
                                         defaultValue={currentPickedProduct.cost}
                                         onChange={(value) => {
                                             setCurrentPickedProduct(prevState => ({
                                                 ...prevState,
                                                 cost: value
                                             }))
                                         }}
                            />
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Скидка"
                            className='loginforminputs'
                            key={'discount'}
                        >
                            <InputNumber value={currentPickedProduct.discount} min={0} max={1} step={0.1}
                                         style={{width: '100%'}} defaultValue={currentPickedProduct.discount}
                                         onChange={(value) => {
                                             setCurrentPickedProduct(prevState => ({
                                                 ...prevState,
                                                 discount: value
                                             }))
                                         }}/>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Горячий продукт"
                            className='loginforminputs'
                            key={'hot'}
                        >
                            <Select defaultValue={currentPickedProduct.hot} value={currentPickedProduct.hot}
                                    onChange={(value) => {
                                        setCurrentPickedProduct(prevState => ({
                                            ...prevState,
                                            hot: value
                                        }))
                                    }}>
                                <Select.Option value={true}>Добавить</Select.Option>
                                <Select.Option value={false}>Не добавлять</Select.Option>
                            </Select>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Фото продукта"
                            className='loginforminputs'
                            key={'photos'}
                        >
                            <Image.PreviewGroup>
                                <Image
                                    width={200}
                                    src={`/images/products/${currentPickedProduct.type}/${currentPickedProduct._id}/preview.jpeg`}
                                    alt={'Нет фото превью'}
                                />
                                <Image
                                    width={200}
                                    src={`/images/products/${currentPickedProduct.type}/${currentPickedProduct._id}/1.jpeg`}
                                    alt={'Нет первого фото'}
                                />
                                <Image
                                    width={200}
                                    src={`/images/products/${currentPickedProduct.type}/${currentPickedProduct._id}/2.jpeg`}
                                    alt={'Нет второго фото'}
                                />
                                <Image
                                    width={200}
                                    src={`/images/products/${currentPickedProduct.type}/${currentPickedProduct._id}/3.jpeg`}
                                    alt={'Нет третьего фото'}
                                />
                            </Image.PreviewGroup>
                        </Item>
                        <Item
                            {...formItemLayout}
                            label="Изменить фото"
                            className='loginforminputs'
                            key={'photoschange'}
                        >
                            {currentPickedPhoto === 'preview' ?
                                <>
                                    <h1>Фотографии должны быть</h1>
                                    <h2>В разрешении 400*400</h2>
                                    <h2>В формате .jpg или .jpeg</h2>
                                </> :
                                <>
                                    <h1>Фотографии должны быть</h1>
                                    <h2>В разрешении 1280*720</h2>
                                    <h2>В формате .jpg или .jpeg</h2>
                                </>
                            }
                            <Select defaultValue={currentPickedPhoto} value={currentPickedPhoto} onChange={(value) => {
                                setCurrentPickedPhoto(value);
                            }}>
                                <Option value="preview">Предпросмотр</Option>
                                <Option value="1">1 Фото</Option>
                                <Option value="2">2 Фото</Option>
                                <Option value="3">3 Фото</Option>
                            </Select>
                            <div style={{
                                marginTop: '10px',
                            }}>
                                <Upload
                                    listType="picture-card"
                                    fileList={file}
                                    onChange={submitPhotoHandler}
                                    beforeUpload={() => false}
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }}
                                >
                                    {uploadButton}
                                </Upload>
                                <Tooltip title="Нажмите чтобы загрузить">
                                    <Button type="primary" icon={<UploadOutlined/>} style={{width:'100%'}} onClick={uploadPhoto}/>
                                </Tooltip>
                            </div>
                        </Item>
                        <Item>
                            <div className='Cv__Submit-Button'>
                                <Button type="primary" onClick={submitHandler}
                                        style={{width: "180px", textAlign: 'center'}}
                                        className='submit-button'>
                                    Изменить товар
                                </Button>
                            </div>
                        </Item>
                    </div>
                </div>}
            </Modal>
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};

export default connect(mapStateToProps, {updateProduct, updateProductPhoto})(ChangeProduct);