import React, {useEffect, useState} from 'react';
import {AutoComplete, Button, message, Modal, Select} from "antd";
import {connect} from "react-redux";
import {changeUserType, getUsers} from "../../../Store/User/adminActions";
import '../AdminModule.css';

const {Option} = AutoComplete;

const ChangeUser = (props) => {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [users, setUsers] = useState([]);
    const [filteredUsers, setFilteredUsers] = useState([]);
    const [currentPickedUser, setCurrentPickedUser] = useState('');
    const [userType, setUserType] = useState('user');

    useEffect(() => {
        props.getUsers().then(res => {
            setUsers(res.data);
            setFilteredUsers(res.data);
        })
        //eslint-disable-next-line
    }, []);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const onSelect = (data) => {
        setCurrentPickedUser(data);
    };

    const changeUserType = () =>{
        props.changeUserType(currentPickedUser, userType).then(res=>{
            message.success(`Пользователь ${res.data.name} изменён на ${res.data.userType}`);
            setCurrentPickedUser('');
            setUserType('user');
        }).catch(err=>{
            message.error('Ошибка в изменении');
            setCurrentPickedUser('');
            setUserType('user');
        })
    };

    const onSearch = (searchid) => {
        let searched = users.filter((user) => user.mobilePhone.indexOf(searchid) > -1);
        setFilteredUsers(searched);
    };

    return (
        <>
            <Button type="primary" onClick={showModal} className='Admin__Button'>
                Изменить пользователя
            </Button>
            <Modal
                title="Изменение пользователя"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
                <div style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <AutoComplete
                        style={{
                            width: 200,
                        }}
                        onSelect={onSelect}
                        onSearch={onSearch}
                        placeholder="Телефон пользователя"
                    >
                        {filteredUsers.map((user) => {
                            return (
                                <Option key={user._id} value={user._id}>{user.name + ' ' + user.surName}</Option>
                            )
                        })}
                    </AutoComplete>
                </div>
                {currentPickedUser ? <div className='changeUserType'>
                    Выбранный пользователь:<b> {currentPickedUser} </b>
                    <div style={{textAlign: 'center', width: '100%'}}>
                        <b>Выберите тип:</b> <Select defaultValue={userType} value={userType} style={{width: '100%'}} onChange={(type)=>{
                        setUserType(type);
                }}>
                    <Option value="user">Пользователь</Option>
                    <Option value="cooker">Повар</Option>
                    <Option value="delivery">Доставщик</Option>
                    <Option value="admin">Администратор</Option>
                </Select>
                        <Button style={{marginTop: '10px'}} type='primary' onClick={changeUserType}>Изменить пользователя</Button>
                </div> </div>: null}
            </Modal>
        </>
    );
};

export default connect(null, {getUsers, changeUserType})(ChangeUser);