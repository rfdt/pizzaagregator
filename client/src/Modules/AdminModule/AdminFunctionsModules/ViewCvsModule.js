import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { Collapse, Image, Modal, Button } from 'antd';
import {viewCvs, markedCvs, deleteCvs} from '../../../Store/User/adminActions'
import '../AdminModule.css';

const { Panel } = Collapse;

const ViewCvs = (props) =>{

    const [cvs, setCvs] = useState([]);
    const [isModalVisible, setIsModalVisible] = useState(false);

    useEffect(()=>{
        props.viewCvs().then(res=>{
            setCvs(res.data);
        })
        // eslint-disable-next-line    
    }, []);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" style={{width: '50%', marginBottom: '15px'}} onClick={showModal} className='Admin__Button'>
                Просмотр резюме
            </Button>
            <Modal
                width ='80%'
                title="Просмотр резюме"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}>
            <div>
                <Collapse accordion>
                    {cvs.length > 0 ? cvs.map((cv)=>{
                        let vacans = cv.cvVacansies === 'delivery' ? 'Доставка' : 'Повар';
                        return (
                        <Panel key={cv._id} header={
                        <div className={cv.marked ? 'panel__header panel__header__marked' : 'panel__header'}>{vacans + ': ' + cv.name + ' ' + cv.surName + ' ' + cv.age + ' лет'}</div>}>
                            <div className='panel__container'>
                                <div className='panel__info'>
                                    <div className='panel__cv'>Вакансия: {vacans}</div>
                                    <div className='panel__name'>{cv.name + ' ' + cv.surName + ' ' + cv.age + ' лет'}</div>
                                    <div className='panel__mobile'>Номер: {cv.mobilePhone}</div>
                                    <div className='panel__desc'>{cv.cvText}</div>
                                    <div className='panel__desc'><Button type='primary' onClick={()=>{
                                        props.markedCvs(cv._id).then(res=>{
                                            setCvs(prev=>prev.map(cv=>{
                                                if (cv._id === res.data._id) {
                                                    return {
                                                        ...cv,
                                                        marked: true
                                                    }
                                                }else {
                                                    return { 
                                                        ...cv
                                                    }
                                                }
                                            }))
                                        })
                                    }}>Отметить заявку</Button></div>
                                    <div className='panel__desc'><Button type='primary'onClick={()=>{
                                        props.deleteCvs(cv._id).then(res=>{
                                            setCvs(prev=>prev.filter((cv)=> cv._id !== res.data._id))
                                        })
                                    }}>Удалить заявку</Button></div>
                                </div>
                                <div className='panel__photo'>
                                <Image
                                    width={'50%'}
                                    src={cv.linkToPhoto}
                                />
                                </div>
                            </div>   
                        </Panel>
                        )
                    })  : <Panel header='Нет резюме'>
                        <p>Нет резюме</p>
                        </Panel>}
                </Collapse>
            </div>
        </Modal>
        </>
    )
}

export default connect(null, {viewCvs, markedCvs, deleteCvs})(ViewCvs);