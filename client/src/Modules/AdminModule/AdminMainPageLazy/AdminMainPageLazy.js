import React, {Suspense} from 'react';
import Loader from "../../Loader/Loader";

const DeliveryOrder = React.lazy(() => import('../AdminModule'));

const AdminMainPageLazy = (props) => {
    return (
        <Suspense fallback={<Loader/>}>
            <DeliveryOrder/>
        </Suspense>
    );
};

export default AdminMainPageLazy;