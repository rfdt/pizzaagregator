import React from 'react';
import AddNewProductModule from "./AdminFunctionsModules/AddNewProductModule";
import AdminStatsModule from "./AdminFunctionsModules/AdminStatsModule";
import ChangeProduct from "./AdminFunctionsModules/ChangeProduct";
import ViewCvsModule from './AdminFunctionsModules/ViewCvsModule';
import './AdminModule.css';
import ChangeUser from "./AdminFunctionsModules/ChangeUser";
import ChangeCarousel from "./AdminFunctionsModules/ChangeCarousel";
import AddNewCoupon from "./AdminFunctionsModules/AddNewCoupon";

const AdminModule = (props) => {
    return (
        <div className='Admin__Container'>
            <div className="Admin__Title">
                Панель Администратора
            </div>
            <div className='Admin__Add-New-Product'>
                <AddNewProductModule/>
                <ChangeProduct/>
            </div>
            <div className='Admin__Add-New-Product'>
                <ChangeUser/>
                <ChangeCarousel/>
            </div>
            <div className="Admin__Stats">
                <AdminStatsModule/>
            </div>
            <div className="Admin__Stats">
                <ViewCvsModule />
                <AddNewCoupon/>
            </div>
        </div>
    );
};

export default AdminModule;