import React from 'react';
import './CartModule.css';
import {Table, Tag} from "antd";
import DeleteOutlined from "@ant-design/icons/DeleteOutlined";
import {connect} from "react-redux";
import {romoveFromCartThunk} from "../../Store/User/userActions";

const CartItem = (props) => {

    const deleteProduct = (serialId) => {

        const editedCart = props.cart.filter(product => product.serialId !== serialId);
        props.romoveFromCartThunk(editedCart)

    };

    return (
        <Table dataSource={props.cart} pagination={false} rowKey='serialId'>
            <Table.Column title={'Название'} dataIndex='name'/>
            <Table.Column title={'Цена'} dataIndex='cost' render={(text) => (`${text}₽`)}/>
            <Table.Column title={'Добавки'} dataIndex='toppings' render={toppings => (
                <>
                    {toppings.length > 0 ? toppings.map(topping => {
                        let color = topping.length > 5 ? 'geekblue' : 'green';
                        return (
                            <div className='topping-container-row'>
                                <Tag color={color} key={topping}>
                                    {topping}
                                </Tag>
                            </div>
                        );
                    }) : <Tag color='cyan'>
                        Нет добавок
                    </Tag>}
                </>
            )}/>
            <Table.Column title={'Ваши предпочтения'} dataIndex='userPreferences' className=''
                          render={(userPreferences) => (
                              <>
                                  {userPreferences ?
                                      <Tag color='purple'>
                                          {userPreferences}
                                      </Tag>
                                      :
                                      <Tag color='purple'>
                                          Нет предпочтений
                                      </Tag>
                                  }
                              </>
                          )}/>
            <Table.Column title='' dataIndex='serialId' render={(serialId) => (
                <DeleteOutlined style={{fontSize: '25px'}} onClick={deleteProduct.bind(null, serialId)}/>
            )}
            />
        </Table>
    );
};

export default connect(null, {romoveFromCartThunk})(CartItem);