import React, {useEffect, useState} from 'react';
import './CartModule.css'
import CartItem from "./CartItem";
import {Empty} from "antd";
import CheckoutModule from "./CheckoutModule/CheckoutModule";
import CouponModule from "./CouponModule/CouponModule";
import {connect} from "react-redux";


const CartModule = (props) => {


    const [cart, setCart] = useState(props.userInfo.cart);
    const [totalCost, setTotalCost] = useState(0);
    const [isUsedCupon, setIsUsedCupon] = useState(false); // для купона

    useEffect(() => {

        let localTotalCost = cart.reduce((sum, current) => {
            return sum + current.cost
        }, 0);

        setTotalCost(localTotalCost);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [cart.length]);

    useEffect(() => {
        setCart(props.userInfo.cart);
        // eslint-disable-next-line
    }, [props.userInfo.cart.length]);

    return (
        <div className='Cart-Module-Container'>
            <div className='Cart-Module-Title'>
                <div>Ваша корзина</div>
                <div className="Cart-Module-Title-Sum">
                    Всего {cart.length} товара
                </div>
            </div>
            <div className='Cart-Module-Products'>
                {cart.length ? <CartItem cart={cart} setCart={setCart}/> : <Empty description='Нет товаров в корзине'/>}
            </div>
            <div className="Cart-Module-Footer">
                <CouponModule cart={cart} setCart={setCart}
                              totalCost={totalCost} setTotalCost={setTotalCost}
                              isUsedCupon={isUsedCupon} setIsUsedCupon={setIsUsedCupon}/>
                <div className='Cart-Module-Title-Sum'>Общая стоимость:&nbsp;<b>{totalCost}₽</b></div>
                <CheckoutModule cart={cart} setCart={setCart}
                                totalCost={totalCost} setTotalCost={setTotalCost}
                                isUsedCupon={isUsedCupon} setIsUsedCupon={setIsUsedCupon}/>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        userInfo: state.user.user
    }
};

export default connect(mapStateToProps, null)(CartModule);