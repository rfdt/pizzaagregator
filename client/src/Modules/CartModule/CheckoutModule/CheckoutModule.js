import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {createOrder} from "../../../Store/Orders/ordersActions";
import {Button, Input, Modal, Popover, Radio, message} from "antd";
import './CheckoutModule.css';

const CheckoutModule = (props) => {

    const [visible, setVisible] = useState(false);
    const [payMethod, setPayMethod] = useState('cash');
    const [userPref, setUserPref] = useState('');
    const [totalCheckoutCost, setTotalCheckoutCost] = useState(props.totalCost);
    const [newAdress, setNewAdress] = useState('standard');
    const [adress, setAdress] = useState(props.user.adresses[0]);


    useEffect(() => {
        if (props.totalCost > 400) {
            setTotalCheckoutCost(props.totalCost);
        } else {
            setTotalCheckoutCost(props.totalCost + 200);
        }
    }, [props.totalCost]);


    const handleCancel = () => {
        setUserPref('');
        setPayMethod('cash');
        setVisible(false);
    };

    const showModal = () => {
        setVisible(true);
    };

    const createCheckout = () => {

        const newCheckout = {
            customerId: props.user.id,
            customerName: props.user.name,
            customerSurName: props.user.surName,
            customerPhone: props.user.mobilePhone,
            customerAdress: adress,
            totalCost: totalCheckoutCost,
            userPreferences: userPref,
            payMethod: payMethod,
            cart: props.cart,
            isUsedCupon: props.isUsedCupon
        };

        props.createOrder(newCheckout).then(res=>{
            message.success('Заказ успешно создан');
            handleCancel();
        }).catch(err=>{
            message.error(`${err.data.msg}`);
        })

    };

    const changeUserPref = (e) => {
        setUserPref(prev => {
            return (e.target.value);
        })
    };

    const setNewAdressText = (e) => {
        setAdress(prev=>{
            return(e.target.value);
        })
    }

    const changePaymentMethod = (e) => {
        if (e.target.value === 'cash') {
            setPayMethod('cash');
        } else if (e.target.value === 'card') {
            setPayMethod('card');
        }
    };

    const changeAdress = (e) =>{
        if (e.target.value === 'standard') {
            setNewAdress('standard');
            setAdress(props.user.adresses[0]);
        } else if (e.target.value === 'new') {
            setNewAdress('new');
        }
    }

    return (
        <>
            <Button type="primary" onClick={showModal} size='large' disabled={!props.totalCost}>
                Оформить заказ
            </Button>
            <Modal
                visible={visible}
                title="Оформление заказа"
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Отменить
                    </Button>
                ]}
            >
                <div>
                    <div className='Pay-Method-Container'>
                        <div className='Pay-Method-Title'>
                            Выберите способ оплаты
                        </div>
                        <Radio.Group defaultValue="cash" value={payMethod} buttonStyle="solid"
                                     onChange={changePaymentMethod} size='large' style={{width: '100%'}}>
                            <Radio.Button value="cash"
                                          className='PayMethod-RadioButtonSize'>Наличными</Radio.Button>
                            <Radio.Button value="card" className='PayMethod-RadioButtonSize'>Картой</Radio.Button>
                        </Radio.Group>
                    </div>
                    <div className='Pay-Method-Container'>
                        <div className='Pay-Method-Title'>
                            Выберите адресс
                        </div>
                        <Radio.Group defaultValue='standard' value={newAdress} buttonStyle="solid"
                                     onChange={changeAdress} size='large' style={{width: '100%'}}>
                            <Radio.Button value="standard" className='Adress-RadioButtonSize'>{props.user.adresses[0]}</Radio.Button>
                            <Radio.Button value="new" className='Adress-RadioButtonSize'>Другой адресс</Radio.Button>
                        </Radio.Group>
                    </div>
                    {
                        newAdress === 'new' && <Input.TextArea className='TextAreaStyle' autoSize={{minRows: 3, maxRows: 3}}
                                                            placeholder='Новый адресс' onChange={setNewAdressText} value={adress}/>
                    }
                    {props.totalCost < 400 &&
                    <Popover content='Бесплатная доставка от 400р' title="Внимание!">
                        <div className='No-Free-Delivery'>
                            ★Стоимость доставки 200₽★
                        </div>
                    </Popover>
                    }
                    <div className='Total-Checkout-Cost'>
                        Общая стоимость {totalCheckoutCost}₽
                    </div>
                    <Input.TextArea className='TextAreaStyle' style={{marginTop: '10px'}} autoSize={{minRows: 3, maxRows: 3}}
                                    placeholder='Напишите пожелания к доставке:
Например: Не поднимайтесь. Позвоните на телефон.' onChange={changeUserPref} value={userPref}/>
                    <div className='Checkout-Submit-Button'>
                        <Button size='large' style={{width: '60%'}} onClick={createCheckout}>Оформить заказ</Button>
                    </div>
                </div>
            </Modal>
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};

export default connect(mapStateToProps, {createOrder})(CheckoutModule);