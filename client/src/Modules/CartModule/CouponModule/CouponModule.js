import React, {useState} from 'react';
import {Button, Modal, Input, message} from 'antd';
import {connect} from 'react-redux';
import {activeCoupon} from "../../../Store/Orders/ordersActions";
import { v4 as uuidv4 } from 'uuid';
import './CouponModule.css';

const CouponModule = (props) => {

    const [visible, setVisible] = useState(false);
    const [couponName, setCouponName] = useState('');

    const handleActive = () => {
        if(couponName) {
            props.activeCoupon(couponName).then(res => {
                couponHandler(res);
            }).catch(err => {
                console.log('Error',err);
            });
        }
    }

    const handleCancel = () => {
        setVisible(false);
        setCouponName('');
    };

    const handleVisible = () => {
        setVisible(true);
    }

    const handleChange = (e) => {
        setCouponName(e.target.value);
    }


    const couponHandler = (coupon) => {
        if (!coupon) {
            return message.error('Ошибка! Нет такого купона.')
        }

        if (coupon.type === 'perc') {
            if (props.totalCost >= coupon.minsum) {
                props.setTotalCost(Math.round(props.totalCost - (props.totalCost * coupon.perc)));
                props.setIsUsedCupon(true);
                handleCancel();
                return message.success(`Активирован купон на ${coupon.perc * 100}%.`)
            } else {
                return message.error(`Минимальная сумма заказа для активации промокода ${coupon.minsum}₽`)
            }
        }

        if (coupon.type === 'sum') {
            if (props.totalCost >= coupon.minsum) {
                props.setTotalCost(props.totalCost - coupon.sum);
                props.setIsUsedCupon(true);
                handleCancel();
                return message.success(`Активирован купон на ${coupon.sum}₽.`)
            } else {
                return message.error(`Минимальная сумма заказа для активации промокода ${coupon.minsum}₽`)
            }
        }

        if(coupon.type === 'gift'){
            if (props.totalCost >= coupon.minsum) {
                props.setCart([...props.cart, {
                    ...coupon.gift,
                    serialId: uuidv4()
                }]);
                props.setIsUsedCupon(true);
                handleCancel();
                return message.success(`Активирован купон на ${coupon.gift.name}.`)
            }else {
                return message.error(`Минимальная сумма заказа для активации промокода ${coupon.minsum}₽`)
            }
        }
    }

    return (
        <>
            <Button disabled={props.isUsedCupon || !props.totalCost} onClick={handleVisible} size='large'>Активировать
                промокод</Button>
            <Modal
                visible={visible}
                title="Активация промокода"
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Отменить
                    </Button>
                ]}>
                <div className='Coupon-Container'>
                    <div className='Coupon-Title'>Введите промокод</div>
                    <div className='Coupon-Input'>
                        <Input placeholder='Промокод сюда' onChange={handleChange} value={couponName}/>
                    </div>
                    <div className='Coupon-Btn'>
                        <Button type='primary' onClick={handleActive}>Активировать</Button>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default connect(null, {activeCoupon})(CouponModule);