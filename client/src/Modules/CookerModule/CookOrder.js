import React from 'react';
import './CookerModule.css';
import cookerMan from "../../images/order/cookerMan.svg";
import {Card} from "antd";
import {pickCookerOrder} from "../../Store/Orders/ordersActions";
import {connect} from "react-redux";
import {withRouter} from "react-router";

const CookOrder = (props) => {

    const clickHandler = () =>{
        props.pickCookerOrder(props.id, props.user.id, props.user.name).then(res=>{
            props.history.push(`/cooker/${res.data.id}`);
        });
    };

    return (
        <div className='Cook-Order-Page'>
            <Card
                hoverable
                style={{width: 240}}
                cover={<img alt="CookOrder" src={cookerMan}/>}
                onClick={clickHandler}
            >
                <Card.Meta title={
                    <div className='Order-Title'>
                        <div>
                            Номер заказа
                        </div>
                        <div style={{fontSize: '12px', fontWeight: 'bold'}}>
                            {props._id}
                        </div>
                    </div>
                } description={<div className='Order-Meta'>
                    <div>
                        Всего: {props.cart.length} товаров
                    </div>
                </div>}/>
            </Card>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};

export default connect(mapStateToProps, {pickCookerOrder})(withRouter(CookOrder));