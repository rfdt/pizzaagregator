import React, {useEffect, useState} from 'react';
import CookOrder from "./CookOrder";
import {Empty} from "antd";
import './CookerModule.css';
import {getSomeOrders} from "../../Store/Orders/ordersActions";
import {connect} from "react-redux";
import Loader from "../Loader/Loader";

const CookerMainPage = (props) => {

    const [cookOrders, setCookOrders] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setLoading(true);
        props.getSomeOrders(0).then(res=>{
            setLoading(false);
            setCookOrders(res.data);
        })
        //eslint-disable-next-line
    }, []);

    return (
        <div className='Cooker-Main-Container'>
            <div className='Cooker-Main-Title'>Меню повара</div>
            <div className='Cooker-Main-SubTitle'> Приветствуем вас в команде поваров</div>
            <div className='Cooker-Main-SubTitle'> Пожалуйста выберите заказ</div>
            {loading && <Loader/>}
            {cookOrders.length ? cookOrders.map(order=>(
                <CookOrder key={order._id} {...order} id={order._id}/>
            )) : <Empty description='Нет заказов. Вы можете передохнуть'/>}
        </div>
    );
};


export default connect(null, {getSomeOrders})(CookerMainPage);