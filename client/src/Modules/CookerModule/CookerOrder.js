import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {finishCook, getCookerOrder} from "../../Store/Orders/ordersActions";
import {withRouter} from "react-router";
import {Button, Table, Tag} from "antd";
import Loader from "../Loader/Loader";
import './CookerModule.css';

const CookerOrder = (props) => {

    const [order, setOrder] = useState({
        _id: '',
        cart: [],
        stage: '1',
    });

    const finishOrder = () =>{
        props.finishCook(order._id).then(()=>{
            props.history.push('/cooker');
        });
    };

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        props.getCookerOrder(props.match.params.id).then(res => {
            setLoading(false);
            res.data.stage !== 1 && props.history.push('/cooker');
            props.user.id !== res.data.cookId && props.history.push('/cooker');
            if (res.data.stage === 1) {
                setOrder(res.data);
            }
        })
        // eslint-disable-next-line
    }, []);

    return (
        <div>
            <div className='Cooker-Order__Title'>Здравствуйте {props.user.name}</div>
            <div className='Cooker-Order__Id'>Заказ №{order._id}</div>
            <div className='Cooker-Order__Sub-title'>Состав заказа</div>
            {loading && <Loader/>}
            <div className='Cooker-Order__Order-Table'>
                {order.cart.length ?
                    <Table style={{marginTop: '10px'}} dataSource={order.cart} pagination={false} rowKey='serialId'>
                        <Table.Column title={'Название'} dataIndex='name'/>
                        <Table.Column title={'Добавки'} dataIndex='toppings' render={toppings => (
                            <>
                                {toppings.length > 0 ? toppings.map(topping => {
                                    let color = topping.length > 5 ? 'geekblue' : 'green';
                                    return (
                                        <div className='topping-container-row'>
                                            <Tag color={color} key={topping}>
                                                {topping}
                                            </Tag>
                                        </div>
                                    );
                                }) : <Tag color='cyan'>
                                    Нет добавок
                                </Tag>}
                            </>
                        )}/>
                        <Table.Column title={'Предпочтения заказчика'} dataIndex='userPreferences' className=''
                                      render={(userPreferences) => (
                                          <>
                                              {userPreferences ?
                                                  <Tag color='purple'>
                                                      {userPreferences}
                                                  </Tag>
                                                  :
                                                  <Tag color='purple'>
                                                      Нет предпочтений
                                                  </Tag>
                                              }
                                          </>
                                      )}/>
                    </Table> : null}
                <Button type='primary' className='Cooker__Finish-Button' onClick={finishOrder}>Завершить приготовление</Button>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};

export default connect(mapStateToProps, {getCookerOrder, finishCook})(withRouter(CookerOrder));