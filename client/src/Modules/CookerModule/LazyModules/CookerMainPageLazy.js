import React, {Suspense} from 'react';
import Loader from "../../Loader/Loader";

const CookerMainPage = React.lazy(() => import('../CookerMainPage'));

const CookerMainPageLazy = (props) => {
    return (
        <>
            <Suspense fallback={<Loader/>}>
                <CookerMainPage/>
            </Suspense>
        </>
    );
};

export default CookerMainPageLazy;