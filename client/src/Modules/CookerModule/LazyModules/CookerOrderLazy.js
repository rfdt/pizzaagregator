import React, {Suspense} from 'react';
import Loader from "../../Loader/Loader";

const CookerOrder = React.lazy(() => import('../CookerOrder'));

const CookerMainPageLazy = (props) => {
    return (
        <>
            <Suspense fallback={<Loader/>}>
                <CookerOrder/>
            </Suspense>
        </>
    );
};

export default CookerMainPageLazy;