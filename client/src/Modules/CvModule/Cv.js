import React from 'react';
import {useFormik} from "formik";
import axios from "axios";
import * as Yup from "yup";
import {Button, Form, Input, message, Select} from "antd";
import {SmileOutlined} from "@ant-design/icons";
import {addCv} from "../../Store/User/userActions";
import {connect} from "react-redux";
import './cv.css';

const {Option} = Select;


export const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 15},
};

const CvSchema = Yup.object().shape({
    name: Yup.string().required('Введите имя'),
    surName: Yup.string().required('Введите фамилию'),
    mobilePhone: Yup.string().required('Введите телефон').matches(
        "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$",
        "Номер должен иметь вид +7 978*******"),
    linkToPhoto: Yup.string().required('Ссылка на фото'),
    cvVacansies: Yup.string().required('Выберите вакансию'),
    cvText: Yup.string().required('Напишите что-то о себе'),
    age: Yup.number().required('Ваш возраст')
});

const Cv = (props) => {

    const formik = useFormik({
        initialValues: {
            name: '',
            surName: '',
            mobilePhone: '',
            cvText: '',
            linkToPhoto: '',
            cvVacansies: 'delivery',
            age: ''
        },
        validationSchema: CvSchema,
        onSubmit: (values , {resetForm}) => {
            const newCv = {
                name: values.name,
                surName: values.surName,
                mobilePhone: values.mobilePhone,
                cvText: values.cvText,
                linkToPhoto: values.linkToPhoto,
                cvVacansies: values.cvVacansies,
                age: values.age
            };
            axios.post('http://localhost:8000/api/cv/create', newCv).then(res => {
                message.success('Ваше резюме отправлено');
                resetForm({});
            }).catch(()=>{
                message.error('Проблема с отправкой резюме');
            })
        },
    });


    return (
        <div className='Cv__Container'>
            <div className="Cv__Title-Container">
                <div className="Cv__Title">
                    Привет
                </div>
                <div className="Cv__Sut-Title">
                    Хочешь работать с нами?
                </div>
                <div className="Cv__Sut-Title">
                    Мы всегда рады видеть новых людей!
                </div>
            </div>
            <div className="Cv__Text">
                У нас есть всегда 2 вакансии: Доставщик и повар
            </div>
            <div className="Cv__Text">
                Оставь заявку! И мы по возможности рассмотрим её!
            </div>
            <div className="Cv__Text">
                И в порядке очереди перезвоним
            </div>
            <div className="Cv__Form-Title">
                ✭Ваше резюме✭
            </div>
            <form onSubmit={formik.handleSubmit} >
                <Form.Item
                    {...formItemLayout}
                    label="Имя"
                    className='loginforminputs'
                    validateStatus={formik.errors.name && formik.touched.name && 'error'}
                    help={formik.touched.name && formik.errors.name}
                >
                    <Input placeholder="Введите имя" value={formik.values.name}
                           onChange={formik.handleChange}
                           name='name' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Фамилия"
                    className='loginforminputs'
                    validateStatus={formik.errors.surName && formik.touched.surName && 'error'}
                    help={formik.touched.surName && formik.errors.surName}
                >
                    <Input placeholder="Введите фамилию" value={formik.values.surName}
                           onChange={formik.handleChange}
                           name='surName' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Возраст"
                    className='loginforminputs'
                    validateStatus={formik.errors.age && formik.touched.age && 'error'}
                    help={formik.touched.age && formik.errors.age}
                >
                    <Input placeholder="Введите возраст" value={formik.values.age}
                           onChange={formik.handleChange}
                           name='age' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Мобильный"
                    className='loginforminputs'
                    validateStatus={formik.errors.mobilePhone && formik.touched.mobilePhone && 'error'}
                    help={formik.touched.mobilePhone && formik.errors.mobilePhone}
                >
                    <Input placeholder="Введите мобильный телефон" value={formik.values.mobilePhone}
                           onChange={formik.handleChange}
                           name='mobilePhone' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Вакансия"
                    className='loginforminputs'
                    validateStatus={formik.errors.cvVacansies && formik.touched.cvVacansies && 'error'}
                    help={formik.touched.cvVacansies && formik.errors.cvVacansies}
                >
                    <Select defaultValue={formik.values.cvVacansies} onChange={(value)=>{formik.setFieldValue('cvVacansies', value)}}>
                        <Option value="delivery">Доставщик</Option>
                        <Option value="cooker">Повар</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="О себе"
                    className='loginforminputs'
                    validateStatus={formik.errors.cvText && formik.touched.cvText && 'error'}
                    help={formik.touched.cvText && formik.errors.cvText}
                >
                    <Input placeholder="Расскажите тут о себе" value={formik.values.cvText}
                           onChange={formik.handleChange}
                           name='cvText' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Фото"
                    className='loginforminputs'
                    validateStatus={formik.errors.linkToPhoto && formik.touched.linkToPhoto && 'error'}
                    help={formik.touched.linkToPhoto && formik.errors.linkToPhoto}
                >
                    <Input placeholder="Вставьте ссылку на фото" value={formik.values.linkToPhoto}
                           onChange={formik.handleChange}
                           name='linkToPhoto' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item>
                    <div className='Cv__Submit-Button'>
                        <Button type="primary" htmlType="submit" style={{width: "180px", textAlign:'center'}} className='submit-button'>
                            Оставить заявку
                        </Button>
                    </div>
                </Form.Item>
            </form>
        </div>
    );
};

export default connect(null, {addCv})(Cv);