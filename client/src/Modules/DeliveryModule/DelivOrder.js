import React from 'react';
import {Card} from "antd";
import food from "../../images/order/food-delivery.svg";
import {connect} from "react-redux";
import { pickDelivOrder} from "../../Store/Orders/ordersActions";
import {withRouter} from "react-router";

const DelivOrder = (props) => {

    const clickHandler = () =>{
        props.pickDelivOrder(props.id, props.user.id, props.user.name, props.user.mobilePhone).then(res=>{
            props.history.push(`/deliv/${res.data.id}`);
        });
    };

    return (
        <div className='Delivery-Order-Page'>
            <Card
                hoverable
                style={{width: 240}}
                cover={<img alt="CookOrder" src={food}/>}
                onClick={clickHandler}
            >
                <Card.Meta title={
                    <div className='Delivery-Order-Page-Title'>
                        <div>
                            Номер заказа
                        </div>
                        <div style={{fontSize: '12px', fontWeight: 'bold'}}>
                            {props._id}
                        </div>
                    </div>
                } description={<div className='Delivery-Order-Page-Meta'>
                    <div>
                        Адрес: {props.customerAdress}
                    </div>
                    <div>Тип оплаты: {props.payMethod === 'card' ? 'Картой. Возьмите терминал' : 'Наличными.'}</div>
                </div>}/>
            </Card>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};

export default connect(mapStateToProps, {pickDelivOrder})(withRouter(DelivOrder));