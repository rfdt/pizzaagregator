import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import Loader from "../Loader/Loader";
import DelivOrder from "./DelivOrder";
import {Empty} from "antd";
import './DeliveryModule.css';
import {getSomeOrders} from "../../Store/Orders/ordersActions";

const DeliveryMainPage = (props) => {

    const [delivOrders, setDelivOrders] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setLoading(true);
        props.getSomeOrders(2).then(res=>{
            setLoading(false);
            setDelivOrders(res.data);
        })
        //eslint-disable-next-line
    }, []);

    return (
        <div className='Delivery-Main-Container'>
            <div className='Delivery-Main-Title'>Меню доставщика</div>
            <div className='Delivery-Main-SubTitle'> Приветствуем вас в команде доставщиков</div>
            <div className='Delivery-Main-SubTitle'> Пожалуйста выберите заказ</div>
            {loading && <Loader/>}
            {delivOrders.length ? delivOrders.map(order=>(
                <DelivOrder key={order._id} {...order} id={order._id}/>
            )) : <Empty description='Нет заказов, готовых к доставке. Но скоро они появятся. Приготовьтесь!'/>}
        </div>
    );
};

export default connect(null, {getSomeOrders})(DeliveryMainPage);