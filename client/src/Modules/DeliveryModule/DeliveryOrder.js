import React, {useEffect, useState} from 'react';
import {Alert, Button} from "antd";
import {connect} from "react-redux";
import {finishDelivery, getCookerOrder} from "../../Store/Orders/ordersActions";
import Loader from "../Loader/Loader";
import {withRouter} from "react-router";
import './DeliveryModule.css'

const DeliveryOrder = (props) => {

    const [order, setOrder] = useState({
        _id: '',
        cart: [],
        stage: '3',
        userPreferences: ''
    });

    const [loading, setLoading] = useState(false);

    const finishOrder = () =>{
        props.finishDelivery(order._id).then(()=>{
            props.history.push('/deliv');
        });
    };

    useEffect(() => {
        setLoading(true);
        props.getCookerOrder(props.match.params.id).then(res => {
            setLoading(false);
            res.data.stage !== 3 && props.history.push('/deliv');
            props.user.id !== res.data.deliverymanId && props.history.push('/deliv');
            if (res.data.stage === 3) {
                setOrder(res.data);
            }
        })
        // eslint-disable-next-line
    }, []);

    return (
        <div>
            <div className='Delivery-Order__Title'>Здравствуйте {props.user.name}</div>
            <div className='Delivery-Order__Id'>Заказ №{order._id}</div>
            <div className='Delivery-Order__Sub-title'>Пожелания заказчика. Пожалуйста учтите их</div>
            {loading && <Loader/>}
            <div className="Delivery-Preferences">
                {order.userPreferences.length > 0 ? <div className='Delivery-Preferences-Container'>
                    <Alert message={order.userPreferences} type='info' showIcon/>
                </div> : <div><Alert message='У заказчика нет пожеланий для доставки' type='info' showIcon/></div>}
                <Button type='primary' className='Delivery__Finish-Button' onClick={finishOrder}>Завершить доставку</Button>
            </div>
        </div>
    );
};


const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};


export default connect(mapStateToProps, {finishDelivery, getCookerOrder})(withRouter(DeliveryOrder));