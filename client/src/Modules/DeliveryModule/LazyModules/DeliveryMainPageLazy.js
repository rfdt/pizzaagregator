import React, {Suspense} from 'react';
import Loader from "../../Loader/Loader";

const DeliveryMainPage = React.lazy(() => import('../DeliveryMainPage'));

const DeliveryMainPageLazy = (props) => {
    return (
        <>
            <Suspense fallback={<Loader/>}>
                <DeliveryMainPage/>
            </Suspense>
        </>
    );
};

export default DeliveryMainPageLazy;