import React, {Suspense} from 'react';
import Loader from "../../Loader/Loader";

const DeliveryOrder = React.lazy(() => import('../DeliveryOrder'));

const DeliveryOrderLazy = (props) => {
    return (
        <>
            <Suspense fallback={<Loader/>}>
                <DeliveryOrder/>
            </Suspense>
        </>
    );
};

export default DeliveryOrderLazy;