import React from 'react';
import './Footer.css';
import {NavLink} from "react-router-dom";
import {FieldTimeOutlined} from "@ant-design/icons";
import VkIcon from '../../images/footer/icon_foo_vk.png';
import YtIcon from '../../images/footer/icon_foo_youtube.png';
import InstIcon from '../../images/footer/icon_foo_insta.png';

const Footer = (props) => {
    return (
        <div className='footer-container'>
            <div className="footer-menu">
                <div className="footer-menu-menu">
                    <b>МЕНЮ</b>
                    <NavLink to='/pizza' className='footer-menu-links'>Пицца</NavLink>
                    <NavLink to='/rolls' className='footer-menu-links'>Роллы</NavLink>
                    <NavLink to='/wok' className='footer-menu-links'>WOK Лапша</NavLink>
                    <NavLink to='/desserts' className='footer-menu-links'>Дессерты</NavLink>
                    <NavLink to='/drinks' className='footer-menu-links'>Напитки</NavLink>
                </div>
                <div className="footer-menu-order">
                    <b>СДЕЛАТЬ ЗАКАЗ</b>
                    <div>
                        <NavLink to='/cart' className='footer-menu-links footer-menu-links-bold'>Можно в
                            корзине</NavLink>
                    </div>
                    <div>
                        <FieldTimeOutlined/> пн-чт с 09:00 - 00:50
                    </div>
                    <div>
                        <FieldTimeOutlined/> пт-вс с 09:00 - 01:50
                    </div>
                </div>
                <div className="footer-menu-socials">
                    МЫ В СОЦСЕТЯХ
                    <div className='footer-menu-socials-icons'>
                        <div className="footer-menu-socials-vk">
                            <a href='https://vk.com/d.brazhnikova2' rel="noreferrer" target='_blank'>
                                <img src={VkIcon} alt='VkIcon' className="footer-menu-socials-vk"/>
                            </a>
                        </div>
                        <div className="footer-menu-socials-inst">
                            <a href='https://www.instagram.com/brazhnikova_dar/' rel="noreferrer" target='_blank'>
                                <img src={InstIcon} alt='YtIcon' className="footer-menu-socials-vk"/>
                            </a>
                        </div>
                        <div className="footer-menu-socials-youtube">
                            <a href='https://www.youtube.com/c/maxkatz1' rel="noreferrer" target='_blank'>
                                <img src={YtIcon} alt='YtIcon' className="footer-menu-socials-vk"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div className="footer-menu-desk">
                    <p>
                        Доставка еды в Симферополе от Hola – это идеальный вариант как для весёлой компании, так и для свидания тет-а-тет.
                        А все почему? Потому что вам не надо тратить время и силы на поиск подходящего кафе или ресторана, вам не надо никуда ехать в любую погоду, подыскивая подходящий транспорт, не надо думать, когда и как вы сможете вернуться домой.
                        Все, что вам надо, уже есть в нашем обширнейшем меню – кухни Европы, Азии и Востока сами приедут к вам в гости.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Footer;