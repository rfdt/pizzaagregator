import React, {useState} from 'react';
import {withRouter} from "react-router";
import {NavLink} from "react-router-dom";
import {Button, Drawer, Popover, message, Badge} from "antd";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {connect} from "react-redux";
import './Header.css';
import UserModule from "./UserModule/UserModule";
import {ReactComponent as Location} from '../../images/menu/location.svg';
import {ReactComponent as MenuIcon} from '../../images/menu/menu.svg';
import {ReactComponent as Call} from '../../images/menu/phone-call.svg';
import {ReactComponent as Cart} from '../../images/menu/cart.svg';
import {ReactComponent as Orders} from '../../images/menu/orders.svg';
import LogoSVG from '../../images/menu/pizza.svg';

const MenuTitle = (props) => {
    return (
        <div className='slider-menu-title-container'>
            <div className='Logo-Menu'>МЕНЮ</div>
        </div>
    )
};

const Logo = (props) => {

    const returnToMain = () => {
        props.history.push('/')
    };

    return (
        <div onClick={returnToMain} className={'Logo-Container'}>
            <img className='App-logo' src={LogoSVG} alt='logo' style={{marginTop: '1px'}}/>
            <div className='Logo-Hola'>HOLA</div>
            <img className='App-logo' src={LogoSVG} alt='logo' style={{marginTop: '1px'}}/>
        </div>
    )
};

const LocationModule = () => {
    return (
        <div className='LocationModule-container'>
            <Popover placement="bottom" title='Ваш город' content={'Пока что только в симферополе'}
                     trigger="click">
                <Button icon={<Location fill={'#404040'} className='SVGIcons'/>} type="link"
                        style={{alignSelf: 'center'}}/>
            </Popover>
            <div className='LocationModule-text'>
                Симферополь
            </div>
        </div>
    )
};

const CallModule = () => {

    const onCopy = (text) => {
        message.success(`Скопирован номер ${text}`);
    };

    return (

        <Popover placement="bottom" title='Позвоните нам' content={() => (
            <>
                <CopyToClipboard text='+7 (978) 555 10 42' onCopy={onCopy}>
                    <span className='CallModule-text-numbers'>+7 (978) 555 10 42</span>
                </CopyToClipboard>

                <CopyToClipboard text='+7 (978) 733 56 52' onCopy={onCopy}>
                    <span className='CallModule-text-numbers'>+7 (978) 733 56 52</span>
                </CopyToClipboard>

                <CopyToClipboard text='+7 (978) 857 00 30' onCopy={onCopy}>
                    <span className='CallModule-text-numbers'>+7 (978) 857 00 30</span>
                </CopyToClipboard>
            </>
        )} trigger="click">
            <Button icon={<Call fill={'#404040'} className='SVGIcons'/>} type="link"
                    style={{alignSelf: 'center', width: '40px', height: '40px'}}/>
        </Popover>
    )
};

const CartModule = (props) => {

    const toCartPush = () => {
        props.history.push('/cart')
    };

    return (
        <div style={{width: '40px', height: '40px', marginTop: '10px'}} onClick={toCartPush}>
            <Badge count={props.user.isAuthenticated && props.user.user.cart.length ? `${props.user.user.cart.length}` : 0} size={'small'} showZero={false} style={{padding: 0}} title={`В вашей корзине ${1} товар`}>
                <Cart fill='#404040' className='SVGIcons' style={{width: '40px', height: '40px', marginTop: '0px'}}/>
            </Badge>
        </div>
    )
};

const OrdersModule = (props) => {

    const toOrdersPush = () => {
        props.history.push('/orders')
    };

    return (
        <Orders fill='#404040' className='SVGIcons' style={{width: '40px', height: '40px', marginTop: '10px'}}
                onClick={toOrdersPush}/>
    )
};

const Header = (props) => {
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    return (
        <div className='menu-container'>
            <div className='menu-logo'>
                <Logo history={props.history}/>
            </div>
            <div className='menu-buttons'>
                <LocationModule/>
                <CallModule/>
                <CartModule history={props.history} user={props.user}/>
                <OrdersModule history={props.history}/>
                <Button type="link" onClick={showDrawer} style={{paddingTop: '10px'}}
                        icon={<MenuIcon style={{width: '40px', height: '40px'}}
                                        fill={'#404040'} className={'SVGIcons'}/>}
                        size='large'/>
                <Drawer
                    title={<MenuTitle/>}
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible}
                >
                    <div className='slider-menu-links-container'>
                        <NavLink to='/' className='slider-menu-links' activeClassName='slider-menu-links-active'>Горячие наборы</NavLink>
                        <NavLink to='/sets' className='slider-menu-links' activeClassName='slider-menu-links-active'>Сеты</NavLink>
                        <NavLink to='/pizza' className='slider-menu-links' activeClassName='slider-menu-links-active'>Пицца</NavLink>
                        <NavLink to='/rolls' className='slider-menu-links' activeClassName='slider-menu-links-active'>Роллы</NavLink>
                        <NavLink to='/frrolls' className='slider-menu-links' activeClassName='slider-menu-links-active'>Запеченные роллы</NavLink>
                        <NavLink to='/temprolls' className='slider-menu-links' activeClassName='slider-menu-links-active'>Темпура роллы</NavLink>
                        <NavLink to='/wok' className='slider-menu-links' activeClassName='slider-menu-links-active'>WOK Лапша</NavLink>
                        <NavLink to='/desserts' className='slider-menu-links' activeClassName='slider-menu-links-active'>Дессерты</NavLink>
                        <NavLink to='/drinks' className='slider-menu-links' activeClassName='slider-menu-links-active'>Напитки</NavLink>
                    </div>
                    <UserModule/>
                </Drawer>
            </div>
        </div>
    );
};


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, null)(withRouter(Header));