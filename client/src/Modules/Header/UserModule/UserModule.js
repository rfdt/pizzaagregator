import React from 'react';
import './UserModule.css'
import {connect} from "react-redux";
import {Button, Popover} from "antd";
import {withRouter} from "react-router";
import stk from 'string-to-color';
import {logout} from "../../../Store/User/userActions";


const UserModule = (props) => {

    const logoutClick = () =>{
        props.logout();
    };

    const toPage = (adress) => {
        props.history.push(`/${adress}`)
    };



    return (
        <>
            {props.user.isAuthenticated && props.user.user.userType === 'cooker' && <div className='Button-Auth-User-Container'>
                <Button onClick={toPage.bind(null, 'cooker')} className='Button-Auth-User' size='large'>Готовить</Button>
            </div>}
            {props.user.isAuthenticated && props.user.user.userType === 'delivery' && <div className='Button-Auth-User-Container'>
                <Button onClick={toPage.bind(null, 'deliv')} className='Button-Auth-User' size='large'>Доставка</Button>
            </div>}
            {props.user.isAuthenticated && props.user.user.userType === 'admin' && <div className='Button-Auth-User-Container'>
                <Button onClick={toPage.bind(null, 'admin')} className='Button-Auth-User' size='large'>Админ-Панель</Button>
            </div>}
            {props.user.isAuthenticated && <div className='User-Module-Container'>
                <div className="User-Module-Info-Container">
                    <Popover content='Нажмите чтобы выйти' title="Внимание">
                        <div className="User-Module-Info-Logo" onClick={logoutClick} style={{backgroundColor: stk(props.user.user.name)}}>
                            {props.user.user.name[0].toUpperCase()}
                        </div>
                    </Popover>
                    <div className='User-Module-Info-Data-Container'>
                        <div className="User-Module-Info-Name">
                            Имя: {props.user.user.name}
                        </div>
                        <div className="User-Module-Info-SurName">
                            Фамилия: {props.user.user.surName}
                        </div>
                        <div className="User-Module-Info-Phone">
                            Телефон: {props.user.user.mobilePhone}
                        </div>
                    </div>
                </div>
            </div>}
            {!props.user.isAuthenticated && <div className='Button-Auth-User-Container'>
                <Button onClick={toPage.bind(null, 'login')} className='Button-Auth-User' size='large'>Авторизоваться</Button>
            </div>}
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps, {logout})(withRouter(UserModule));