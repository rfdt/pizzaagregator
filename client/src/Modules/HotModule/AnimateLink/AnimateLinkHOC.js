import React, {useState} from 'react';
import {NavLink} from "react-router-dom";
import ('../HotModule.css');

const AnimateLinkHoc = (props) => {

    const [linknameArray] = useState(props.linkName.split(' '));

    return (
        <NavLink to={props.linkPath} className='AnimateLink__Container' key={props.keyvalue}>
            <img src={props.imgSRC} alt='Icons Products' className='AnimatedLink__Image'/>
            {linknameArray.length > 1 ? <div className='AnimateLink__title'>
                {linknameArray.map((nameFragment)=>(<div key={nameFragment}>{nameFragment}</div>))}
            </div> : <div className='AnimateLink__title'>{props.linkName}</div>}
        </NavLink>
    );
};

export default AnimateLinkHoc;