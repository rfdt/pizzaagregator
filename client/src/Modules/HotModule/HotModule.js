import React from 'react';
import './HotModule.css';
import {Carousel} from "react-responsive-carousel";
import {NavLink} from "react-router-dom";
import HotModuleMenu from "./HotModuleMenu";
import HotProductsItems from "./HotProductsItems";
import hotModulesPay from '../../images/hotmodules/payMethod.png';


const HotModule = (props) => {
    return (
        <div className='Hot-Module-container'>
            <div className="Hot-Module-title">
                Горячие предложения
            </div>
            <div className="Hot-Module-Carousel">
                <Carousel autoplay showThumbs={false} transitionTime={250} axis="vertical" infiniteLoop>
                    <div>
                        <img className='hotContentStyle'
                             src={`/images/hot/carousel/car_1.jpg`}
                             alt={`${props.name}.jpeg`}/>
                    </div>
                    <div>
                        <img className='hotContentStyle'
                             src={`/images/hot/carousel/car_2.jpg`}
                             alt={`${props.name}.jpeg`}/>
                    </div>
                    <div>
                        <img className='hotContentStyle'
                             src={`/images/hot/carousel/car_3.jpg`}
                             alt={`${props.name}.jpeg`}/>
                    </div>
                </Carousel>
            </div>
            <div className="Hot-Module-body-title">
                Доставка в Симферополе
            </div>
            <div className="Hot-Module-menu-title">
                Меню
            </div>
           <HotModuleMenu/>
            <HotProductsItems/>
            <div className="Hot-Module-payment-container">
                <div className="Hot-Module-payment-text">
                    <div className='Hot-Module-payment-text-condition'>Условия доставки:</div>
                    <div className='Hot-Module-payment-text-maininfo'>
                        <div>Доставка пн-чт с 9.00 - 0.50</div>
                        <div>Доставка пт-вс с 9.00 - 1.50</div>
                        <div>Доставка бесплатная по городу от 400 руб.</div>
                        <div>В случае заказа на меньшую сумму - стоимость доставки 200 руб.</div>
                    </div>
                    <div className='Hot-Module-payment-text-paymethod'>Мы принимаем:</div>
                </div>
                <div className="Hot-Module-payment-logos">
                    <img src={hotModulesPay} alt="hotModulesPay method"/>
                    *Внимание при заказе менее 400р доставка платная
                </div>
            </div>
            <div className='Hot-Module-cart'>
                <NavLink to='/cart' className='slider-menu-links-cart'>Ваша корзина</NavLink>
            </div>
            <div className='Hot-Module-work'>
                <NavLink to='/work' className='slider-menu-links-work'>Приходи к нам работать</NavLink>
            </div>
        </div>
    );
};

export default HotModule;