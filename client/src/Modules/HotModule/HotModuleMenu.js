import React from 'react';
import './HotModule.css';
import AnimateLinkHoc from "./AnimateLink/AnimateLinkHOC";
import SetsIcon from '../../images/hotmodules/hotmodulesmenuicons/sets.jpg';
import WokIcon from '../../images/hotmodules/hotmodulesmenuicons/woks.jpg';
import RollsIcon from '../../images/hotmodules/hotmodulesmenuicons/rolls.jpg';
import FriedRollsIcons from '../../images/hotmodules/hotmodulesmenuicons/frrolls.jpg';
import PizzaIcons from '../../images/hotmodules/hotmodulesmenuicons/pizza.jpg';
import TempRollsIcons from '../../images/hotmodules/hotmodulesmenuicons/temprolls.jpeg';
import DrinksIcons from '../../images/hotmodules/hotmodulesmenuicons/drinks.jpg';
import DessertsIcons from '../../images/hotmodules/hotmodulesmenuicons/desserts.jpg';

const HotModuleMenu = (props) => {
    return (
        <div className="Hot-Module-menu">
            <div className="HotModuleMenu__Item" key='first'>
                <AnimateLinkHoc keyvalue='sets' linkPath={'/sets'} imgSRC={SetsIcon} linkName='Сеты'/>
                <AnimateLinkHoc keyvalue='wok' linkPath={'/wok'} imgSRC={WokIcon} linkName='Wok Лапша'/>
                <AnimateLinkHoc keyvalue='rolls' linkPath={'/rolls'} imgSRC={RollsIcon} linkName='Роллы'/>
                <AnimateLinkHoc keyvalue='pizza' linkPath={'/pizza'} imgSRC={PizzaIcons} linkName='Пицца'/>
            </div>
            <div className="HotModuleMenu__Item" key='second'>
                <AnimateLinkHoc keyvalue='frrolls' linkPath={'/frrolls'} imgSRC={FriedRollsIcons} linkName='Запеченные Роллы'/>
                <AnimateLinkHoc keyvalue='temprolls' linkPath={'/temprolls'} imgSRC={TempRollsIcons} linkName='Темпура Роллы'/>
                <AnimateLinkHoc keyvalue='drinks' linkPath={'/drinks'} imgSRC={DrinksIcons} linkName='Напитки'/>
                <AnimateLinkHoc keyvalue='desserts' linkPath={'/desserts'} imgSRC={DessertsIcons} linkName='Дессерты'/>
            </div>
        </div>
    );
};

export default HotModuleMenu;