import React from 'react';
import {connect} from 'react-redux';
import Product from "../Product/Product";
import Loader from "../Loader/Loader";
import './HotModule.css';

const HotProductsItems = (props) => {
    return (
        <div className='Hot-Poduct-Items-Container'>
            {props.hotProducts.isLoading && <Loader />}
            {props.hotProducts.hotProducts.map((product)=>(
                <Product discount={product.discount}
                         cost={product.cost}
                         key={product._id}
                         type={product.type}
                         name={product.name}
                         shortdesk={product.shortdesk}
                         desk={product.desk}
                         id={product._id}
                />
            ))}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        hotProducts: state.hotProducts
    }
};
export default connect(mapStateToProps, null)(HotProductsItems);