import React from 'react';
import {Spin} from "antd";

const Loader = (props) => {
    return (
        <div className='loader-Container'>
            <Spin size='large'/>
        </div>
    );
};

export default Loader;