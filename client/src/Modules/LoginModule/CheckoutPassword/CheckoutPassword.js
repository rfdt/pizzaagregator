import React, {useState} from 'react';
import {Button, Form, Input, Modal, message, Alert} from "antd";
import {SmileOutlined} from "@ant-design/icons";
import {useFormik} from "formik";
import * as Yup from "yup";
import {asyncUserCheckout} from "../../../Store/User/userActions";
import {connect} from "react-redux";

const NewPassSchema = Yup.object().shape({
    name: Yup.string().required('Введите имя'),
    surName: Yup.string().required('Введите фамилию'),
    email: Yup.string().email('Введите почту').required('Обязательное поле'),
    newPassword: Yup.string().required('Введите пароль').matches(
        "^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!#$%&? \"]).*$",
        "Пароль должен содержать 8 символов. \n Одно число и один специальный знак"
    ),
    newPasswordConfirm: Yup.string().required("Повторите пароль").oneOf([Yup.ref("newPassword"), null], "Пароли не совпадают")
});

const CheckoutPassword = (props) => {

    const succes = (res) => {
        message.success('Пароль успешно восстановлен');
    };
    const error = (err) => {
        message.error(`Ошибка: ${err}`);
    };

    const [isVisible, setIsVisible] = useState(false)

    const openModal = () => {
        setIsVisible(true);
    };
    const handleCancel = () => {
        setIsVisible(false);
    };

    const formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 15},
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            surName: '',
            email: '',
            newPassword: '',
            newPasswordConfirm:  ''
        },
        validationSchema: NewPassSchema,
        onSubmit: values => {
            new Promise((resolve, reject)=>{
                props.asyncUserCheckout(values, resolve, reject)
            }).then(res=>{
               succes(res);
            }).catch(err=>{
                error(err.response.data.msg);
            });
        },
    });

    return (
        <>
            <Button type="primary" htmlType="submit" style={{width: "150px", marginTop:'10px'}} className='submit-button' onClick={openModal}>
                Забыли пароль?
            </Button>
            <Modal
                visible={isVisible}
                title="Восстановление пароля"
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Закрыть
                    </Button>,
                ]}
            >
                <form onSubmit={formik.handleSubmit} className='Login-form'>
                    <Form.Item
                        {...formItemLayout}
                        label="Имя"
                        className='loginforminputs'
                        validateStatus={formik.errors.name && formik.touched.name && 'error'}
                        help={formik.touched.name && formik.errors.name}
                    >
                        <Input placeholder="Введите имя" value={formik.values.name}
                               onChange={formik.handleChange}
                               name='name' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        className='loginforminputs'
                        label="Фамилия"
                        validateStatus={formik.errors.surName && formik.touched.surName && 'error'}
                        help={formik.touched.surName && formik.errors.surName}
                    >
                        <Input placeholder="Введите вашу фамилию" value={formik.values.surName}
                               onChange={formik.handleChange}
                               name='surName' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        className='loginforminputs'
                        label="Почта"
                        validateStatus={formik.errors.email && formik.touched.email && 'error'}
                        help={formik.touched.email && formik.errors.email}
                    >
                        <Input placeholder="Введите пароль" value={formik.values.email}
                               onChange={formik.handleChange}
                               name='email' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        className='loginforminputs'
                        label="Новый пароль"
                        validateStatus={formik.errors.newPassword && formik.touched.newPassword && 'error'}
                        help={formik.touched.newPassword && formik.errors.newPassword}
                    >
                        <Input placeholder="Введите новый пароль" value={formik.values.newPassword}
                               onChange={formik.handleChange}
                               name='newPassword' prefix={<SmileOutlined/>} onBlur={formik.handleBlur} type='password'/>
                    </Form.Item>
                    <Form.Item
                        {...formItemLayout}
                        className='loginforminputs'
                        label="Еще раз"
                        validateStatus={formik.errors.newPasswordConfirm && formik.touched.newPasswordConfirm && 'error'}
                        help={formik.touched.newPasswordConfirm && formik.errors.newPasswordConfirm}
                    >
                        <Input placeholder="Пвоторите новый пароль" value={formik.values.newPasswordConfirm}
                               onChange={formik.handleChange}
                               name='newPasswordConfirm' prefix={<SmileOutlined/>} onBlur={formik.handleBlur} type='password'/>
                    </Form.Item>
                    <Form.Item>
                        <div className='submit-button'>
                            <Button type="primary" htmlType="submit" style={{width: "150px"}} className='submit-button'>
                                Восстановить пароль
                            </Button>
                        </div>
                    </Form.Item>
                    <Form.Item>
                        {
                            props.error.id === "LOGIN_FAIL" && <Alert
                                        message="Ошибка"
                                        description={props.error.msg}
                                        type="error"
                                        showIcon
                                        closable
                                        style={
                                            {
                                                width: '100%',
                                                marginTop: '10px',
                                                marginBottom: '10px'
                                            }
                                        }
                                    />
                        }
                    </Form.Item>
                </form>
            </Modal>
        </>
    );
};

const mapStateToProps = state => {
    return {
        error: state.error
    }
};


export default connect(mapStateToProps, {asyncUserCheckout})(CheckoutPassword);