import React from 'react';
import {Button, Form, Input, Alert} from "antd";
import {SmileOutlined} from "@ant-design/icons";
import {useFormik} from "formik";
import * as Yup from "yup";
import '../../App.css';
import {connect} from "react-redux";
import {loginUserThunk} from "../../Store/User/userActions";
import {NavLink} from "react-router-dom";
import CheckoutPassword from "./CheckoutPassword/CheckoutPassword";

const SignupSchema = Yup.object().shape({
    email: Yup.string().email('Введите почту').required('Обязательное поле'),
    password: Yup.string().required('Введите пароль')
});

const Login = (props) => {

    const formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 15},
    };

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: SignupSchema,
        onSubmit: values => {
            props.loginUserThunk(values);
        },
    });


    return (
        <div className="auth-form-container">
            <h1>Авторизация</h1>
            <form onSubmit={formik.handleSubmit} className='Login-form'>
                <Form.Item
                    {...formItemLayout}
                    label="Почта"
                    className='loginforminputs'
                    validateStatus={formik.errors.email && formik.touched.email && 'error'}
                    help={formik.touched.email && formik.errors.email}
                >
                    <Input placeholder="Введите почту" value={formik.values.email}
                           onChange={formik.handleChange}
                           name='email' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    className='loginforminputs'
                    label="Пароль"
                    validateStatus={formik.errors.password && formik.touched.password && 'error'}
                    help={formik.touched.password && formik.errors.password}
                >
                    <Input placeholder="Введите пароль" value={formik.values.password}
                           onChange={formik.handleChange}
                           type='password'
                           name='password' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item>
                    <div className='submit-button'>
                        {
                            props.error.id === "LOGIN_FAIL" ?
                                props.error.msg.split("ID:")[0].trim() === 'Вы не подтвердили номер телефона.' ?
                                    <Alert
                                        message="Ошибка"
                                        description={<div>{props.error.msg.split('ID:')[0]} <NavLink to={`/verify/${props.error.msg.split('ID:')[1]}`}>Нажмите сюда</NavLink></div>}
                                        type="error"
                                        showIcon
                                        closable
                                        style={
                                            {
                                                width: '100%',
                                                marginTop: '10px',
                                                marginBottom: '10px'
                                            }
                                        }
                                    /> : <Alert
                                        message="Ошибка"
                                        description={props.error.msg}
                                        type="error"
                                        showIcon
                                        closable
                                        style={
                                            {
                                                width: '100%',
                                                marginTop: '10px',
                                                marginBottom: '10px'
                                            }
                                        }
                                    />
                                : null
                        }
                        <Button type="primary" htmlType="submit" style={{width: "150px"}} className='submit-button'>
                            Войти
                        </Button>
                        <CheckoutPassword/>
                        <NavLink to='/register'>Нет аккаунта? Зарегистрироваться.</NavLink>
                    </div>
                </Form.Item>
            </form>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        error: state.error
    }
};

export default connect(mapStateToProps, {loginUserThunk})(Login);