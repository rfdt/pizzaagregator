import React from 'react';
import {Route, Switch} from "react-router";
import Products from "../Products/Products";
import HotModule from "../HotModule/HotModule";

const Main = (props) => {
    return (
            <Switch>
                <Route path={'/'} exact component={HotModule}/>
                <Route path={'/:type'} exact component={Products} />
            </Switch>
    );
};

export default Main;