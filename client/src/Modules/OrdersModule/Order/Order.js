import React, {useState} from 'react';
import {Card, Modal} from 'antd';
import orderIcon from '../../../images/order/order.svg';
import NullStage from "./OrderStage/NullStage/NullStage";
import OrderBar from "./OrderBar/OrderBar";
import './Order.css';
import FirstStage from "./OrderStage/FirstStage/FirstStage";
import OrderCart from "../OrderCart/OrderCart";
import SecondStage from "./OrderStage/SecondStage/SecondStage";
import ThirdStage from "./OrderStage/ThirdStage/ThirdStage";
import FourthStage from "./OrderStage/FourthStage/FourthStage";


const Order = (props) => {

    const [visible, setVisible] = useState(false);

    const showModal = () => {
        setVisible(true);
    };

    const handleCancel = () => {
        setVisible(false);
    };

    return (
        <div className='Order-Container'>
            <Card
                hoverable
                style={{width: 240}}
                cover={<img alt="Order" src={orderIcon}/>}
                onClick={showModal}
            >
                <Card.Meta title={
                    <div className='Order-Title'>
                        <div>
                            Номер заказа
                        </div>
                        <div style={{fontSize: '12px', fontWeight: 'bold'}}>
                            {props._id}
                        </div>
                    </div>
                } description={<div className='Order-Meta'>
                    <div>
                        {props.customerAdress}
                    </div>
                    <div>
                        Всего: {props.cart.length} позиций
                    </div>
                    <div>
                        Стоимость: <b>{props.totalCost} ₽</b>
                    </div>
                    <div>
                        Промокод: <b>{props.isUsedCupon ? "Активирован" : "Не активирован"} </b>
                    </div>
                </div>}/>
            </Card>
            <Modal
                centered
                title={props.name}
                visible={visible}
                onCancel={handleCancel}
                footer={[]}
                bodyStyle={{width: 'auto', paddingTop: '25px'}}
            >
                <div className='Order-Modal-Container'>
                    <div className='Order-Modal-Title'>Заказ №:{props._id}</div>
                    <OrderBar stage={props.stage}/>
                    <OrderCart cart={props.cart}/>
                    {props.stage === 0 && <NullStage />}
                    {props.stage === 1 && <FirstStage name={props.cookName} id={props.cookId}/>}
                    {props.stage === 2 && <SecondStage pref={props.userPreferences}/>}
                    {props.stage === 3 && <ThirdStage name={props.deliverymanName} id={props.deliverymanId} phone={props.deliverymanPhone}/>}
                    {props.stage === 4 && <FourthStage id={props._id}/>}
                </div>
            </Modal>
        </div>
    );
};

export default Order;