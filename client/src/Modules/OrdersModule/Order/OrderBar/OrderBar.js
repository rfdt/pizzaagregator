import React from 'react';
import {Steps} from 'antd';
import BakeIcon from '../OrderIcon/BakeIcon';
import DeliveryIcon from '../OrderIcon/DeliveryIcon';
import DeliveredIcon from '../OrderIcon/DeliveredIcon';
import CookedIcon from '../OrderIcon/CookedIcon';
import CreatedIcon from '../OrderIcon/CreatedIcon';

const OrderBar = (props) => {
    return (
        <Steps current={props.stage}>
            <Steps.Step icon={<CreatedIcon/>}/>
            <Steps.Step icon={<BakeIcon/>}/>
            <Steps.Step icon={<CookedIcon/>}/>
            <Steps.Step icon={<DeliveryIcon/>}/>
            <Steps.Step icon={<DeliveredIcon/>}/>
        </Steps>
    );
}

export default OrderBar;