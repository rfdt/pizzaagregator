import React from 'react';
import Icon from '@ant-design/icons';
import {ReactComponent as BakeSvg} from '../../../../images/order/bake.svg';
import 'antd/dist/antd.css';

const BakeIcon = props => <Icon component={BakeSvg} {...props} />;

export default BakeIcon;