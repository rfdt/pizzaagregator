import React from 'react';
import Icon from '@ant-design/icons';
import {ReactComponent as CookedSvg} from '../../../../images/order/cooked.svg';
import 'antd/dist/antd.css';

const CookedIcon = props => <Icon component={CookedSvg} {...props} />;

export default CookedIcon;   