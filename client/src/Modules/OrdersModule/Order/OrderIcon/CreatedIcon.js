import React from 'react';
import Icon from '@ant-design/icons';
import {ReactComponent as CreatedSvg} from '../../../../images/order/created.svg';
import 'antd/dist/antd.css';

const CreatedIcon = props => <Icon component={CreatedSvg} {...props} />;

export default CreatedIcon;   