import React from 'react';
import Icon from '@ant-design/icons';
import {ReactComponent as DeliveredSvg} from '../../../../images/order/delivered.svg';
import 'antd/dist/antd.css';

const DeliveredIcon = props => <Icon component={DeliveredSvg} {...props} />;

export default DeliveredIcon;   