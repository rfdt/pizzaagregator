import React from 'react';
import Icon from '@ant-design/icons';
import {ReactComponent as DeliverySvg} from '../../../../images/order/deliveryMan.svg';
import 'antd/dist/antd.css';

const DeliveryIcon = props => <Icon component={DeliverySvg} {...props} />;

export default DeliveryIcon;   