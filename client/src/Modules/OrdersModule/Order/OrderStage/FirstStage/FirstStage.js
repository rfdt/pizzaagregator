import React from 'react';
import '../OrderStage.css'

const FirstStage = (props) => {
    return (
        <div className='Stage-Container'>
            <div className="firststage-title">
                Повар уже готовит ваш заказ!
            </div>
            <div className="firststage-data">
                Имя повара: <b>{props.name}</b>
            </div>
        </div>
    );
};

export default FirstStage;