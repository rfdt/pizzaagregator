import React, {useState} from 'react';
import {rateOrders} from "../../../../../Store/Orders/ordersActions";
import {Rate, Button} from 'antd';
import {connect} from 'react-redux';
import '../OrderStage.css';

const FourthStage = (props) => {

    const [rate, setRate] = useState(5);

    const handleRateService = (value) =>{
        setRate(value);
    };

    const rateThunk = () =>{
        props.rateOrders(props.id, rate);
    }

    return (
        <div className='Stage-Container'>
            <div className="FourthStage-title">
                <div>Ваш заказ уже у вас</div>
                <div>&#9733;Приятного аппетита&#9733;</div>
            </div>
            <div className="FourthStage-content">
                <div>Если вам не сложно!</div>
                <div>Оцените нас: <Rate allowHalf value={rate} onChange={handleRateService}/></div>
                <Button type='primary' style={{marginTop: '10px', width: '60%'}} onClick={rateThunk}>Оценить</Button>
            </div>
        </div>
    );
};

export default connect(null, {rateOrders})(FourthStage);