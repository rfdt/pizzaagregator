import React from 'react';
import '../OrderStage.css';

const NullStage = (props) => {
    return (
        <div className='Stage-Container'>
           <div className="nullstage-title">
               <div>Ваш заказ только создан</div>
               <div>Мы уже ищем повара, чтобы его приготовить</div>
           </div>
        </div>
    );
};

export default NullStage;