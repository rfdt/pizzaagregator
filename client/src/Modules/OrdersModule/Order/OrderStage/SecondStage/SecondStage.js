import React from 'react';
import '../OrderStage.css'

const SecondStage = (props) => {
    return (
        <div className='Stage-Container'>
            <div className="firststage-title">
                Ваш заказ готов и ждёт доставщика!
            </div>
            <div className="secondstage-content">
                <div className='secondstage-content-title'>
                    <b>Он обязательно учтет ваши пожелания</b>
                </div>
                <div className='secondstage-content-pref'>
                    {props.pref}
                </div>
            </div>
        </div>
    );
};

export default SecondStage;