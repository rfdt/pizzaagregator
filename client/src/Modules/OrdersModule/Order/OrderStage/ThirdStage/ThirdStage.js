import React from 'react';
import '../OrderStage.css';

const ThirdStage = (props) => {
    return (
        <div className='Stage-Container'>
            <div className="firststage-title">
                Доставщик уже едет к вам!
            </div>
            <div className="firststage-data">
                <div>
                    Имя доставщика: <b>{props.name}</b>
                </div>
                <div>
                    Номер доставщика: <b>{props.phone}</b>
                </div>
            </div>
        </div>
    );
};

export default ThirdStage;