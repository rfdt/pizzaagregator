import React from 'react';
import {Tag} from 'antd';
import '../OrdersModule.css';

const OrderCart = (props) => {
    return (
        <div className='Order-Cart-Container'>
            <b>Состав Заказа</b>
            {props.cart.map((product)=>(
                <Tag color='purple' key={product.serialId}>
                    {product.name}
                </Tag>
            ))}
        </div>
    );
};

export default OrderCart;