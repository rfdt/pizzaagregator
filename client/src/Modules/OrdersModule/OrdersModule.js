import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {getOrders} from "../../Store/Orders/ordersActions";
import {Empty} from 'antd';
import Order from './Order/Order';
import './OrdersModule.css';

const OrdersModule = (props) => {

    useEffect(() => {
        props.getOrders();
        // eslint-disable-next-line
    }, []);

    return (
        <div className='Orders-Module-Container'>
            <div className="Orders-Module-title">
                Ваши заказы
            </div>
            {!props.orders.length && <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                    height: 60,
                }}
                description='У вас нет заказов'
            />}
            {props.orders.length ? props.orders.map((order)=>(
                <Order key={order._id} {...order}/>
            )) : null}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        orders: state.orders.orders
    }
};

export default connect(mapStateToProps, {getOrders})(OrdersModule);