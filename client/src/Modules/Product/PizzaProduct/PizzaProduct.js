import React, {useState} from 'react';
import './PizzaProduct.css'
import {Carousel} from "react-responsive-carousel";
import {Radio, Collapse, Input, Transfer} from 'antd';
import {coeffs} from "../../../Utils/productCoeffs";
import {AddonsArray} from "../../../Utils/toppingsTypes";


const PizzaProduct = (props) => {

    const [selectedKeys, setSelectedKeys] = useState([]);

    const changeSize = (e) => {
        if (e.target.value === 'small') {
            props.SetProductCase(prev => {
                return ({
                    ...prev,
                    cost: Math.round(props.cost * props.discount) + (prev.toppings.length * 20),
                    size: 'small'
                })
            })
        } else if (e.target.value === 'big') {
            props.SetProductCase(prev => {
                return ({
                    ...prev,
                    cost: Math.round((props.cost * props.discount * coeffs[props.type].up) - (coeffs[props.type].down * props.cost * props.discount)) + (prev.toppings.length * 20),
                    size: 'big'
                })
            })
        }
    };

    const handleChangeAddons = (nextTargetKeys) => {
        props.SetProductCase(prev => {
            return ({
                ...prev,
                toppings: nextTargetKeys
            });
        });
    };

    const handleSelectChangeAddons = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    };

    const changeUserPref = (e) => {
        props.SetProductCase(prev => {
            return ({
                ...prev,
                userPreferences: e.target.value
            });
        })
    };

    return (
        <>
            <Carousel autoplay showThumbs={false}>
                <div>
                    <img className='contentStyle'
                         src={`/images/products/${props.type}/${props.id}/1.jpeg`}
                         alt={`${props.name}.jpeg`}/>
                </div>
                <div>
                    <img className='contentStyle'
                         src={`/images/products/${props.type}/${props.id}/2.jpeg`}
                         alt={`${props.name}.jpeg`}/>
                </div>
                <div>
                    <img className='contentStyle'
                         src={`/images/products/${props.type}/${props.id}/3.jpeg`}
                         alt={`${props.name}.jpeg`}/>
                </div>
            </Carousel>
            <div className="PizzaProduct-container">
                <div className="PizzaProduct-head">
                    <div className="PizzaProduct-cost">
                        {props.ProductCase.cost}₽
                    </div>
                    {props.type !== 'sets' ?
                        <div className="PizzaProduct-size">
                            Выберите размер
                            <Radio.Group defaultValue="small" value={props.ProductCase.size} style={{width: '100%'}}
                                         size="large" buttonStyle="solid"
                                         onChange={changeSize}>
                                <Radio.Button className='RadioButtonSize' value="small">
                                    {(props.type === 'rolls' || props.type === 'frrolls' || props.type === 'temprolls')  && '12 штук'}
                                    {props.type === 'pizza' && '35СМ'}
                                    {props.type === 'drinks' && '1 Литр'}
                                    {(props.type === 'wok' || props.type === 'desserts') && 'Маленький'}
                                </Radio.Button>
                                <Radio.Button className='RadioButtonSize' value="big">
                                    {(props.type === 'rolls' || props.type === 'frrolls' || props.type === 'temprolls') && '16 штук'}
                                    {props.type === 'pizza' && '50СМ'}
                                    {props.type === 'drinks' && '2 Литр'}
                                    {(props.type === 'wok' || props.type === 'desserts') && 'Большой'}
                                </Radio.Button>
                            </Radio.Group>
                        </div>
                        : null}
                    <div className="PizzaProduct-description">
                        <Collapse accordion className='PizzaProduct-description-collapse'>
                            <Collapse.Panel header={props.shortdesk}>
                                {props.desk}
                            </Collapse.Panel>
                            {props.type === 'drinks' || props.type === 'wok' || props.type === 'desserts' || props.type ==='sets' ?
                                null
                                : <Collapse.Panel header='Дополниельные начинки'>
                                    <div className='addonsCollapsePanel-div'>
                                        <div className='addonsCollapsePanel-div-alert'>&#9733;Каждая начинка
                                            20₽&#9733;</div>
                                        <Transfer
                                            dataSource={AddonsArray[props.type]}
                                            titles={['', '']}
                                            selectedKeys={selectedKeys}
                                            targetKeys={props.ProductCase.toppings}
                                            onChange={handleChangeAddons}
                                            onSelectChange={handleSelectChangeAddons}
                                            render={item => item.key}
                                            oneWay
                                            style={{marginBottom: 16}}
                                        />
                                    </div>
                                </Collapse.Panel>
                            }
                        </Collapse>
                    </div>
                </div>
                <Input.TextArea className='TextAreaStyle' style={{marginTop: '10px'}}
                                autoSize={{minRows: 3, maxRows: 3}}
                                placeholder='Напишите нам свои вкусовые предпочтения:
Например: Меньше сыра.' onChange={changeUserPref} value={props.ProductCase.userPreferences}/>
            </div>
        </>
    );
};

export default PizzaProduct;