import React, {useEffect, useState} from 'react';
import './Product.css'
import {Button, Card, message, Modal, Popover} from 'antd';
import {v4 as uuidv4} from 'uuid';
import PizzaProduct from "./PizzaProduct/PizzaProduct";
import {connect} from "react-redux";
import {addToCartThunk} from "../../Store/User/userActions";
import {coeffs} from "../../Utils/productCoeffs";


const PopoverContent = (props) => {
    return (
        <div className='PopoverContent'>
            {props.type === 'pizza' ? 'Цена указана за пиццу 35см.' : (props.type === 'rolls' || props.type === 'frrolls' || props.type === 'temprolls') ? 'Цена указана за 12шт.' : 'Цена указана за маленький вариант.'}
            {props.discount < 1 && ` Ваша скидка ${Math.round(props.cost * (1 - props.discount))}₽`}
        </div>
    )
};

const ProductTitle = (props) => {
    return (
        <div className='Card-Meta-title'>
            <div className='Card-Meta-desk'>{props.name.length > 15 ? props.name.split(' ').map((word) => (
                <div key={word}>{word}</div>)) : props.name}</div>
            <div style={{color: props.discount < 1 ? 'red' : 'black', textAlign: 'center'}} className='CardCostPanel'>
                <Popover
                    placement="topLeft"
                    title='Внимание!'
                    content={<PopoverContent discount={props.discount} type={props.type} cost={props.cost}/>}>
                    {Math.round(props.cost * props.discount)}₽</Popover></div>
        </div>)
};

const Product = (props) => {

    const initialState = {
        type: props.type,
        name: props.name,
        id: props.id,
        cost: props.cost * props.discount,
        discount: props.discount,
        size: 'small',
        userPreferences: '',
        toppings: []
    };

    const [visible, setVisible] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [ProductCase, SetProductCase] = useState(initialState);

    useEffect(() => {
        SetProductCase(prev => {
            if (ProductCase.size === 'small') {
                return ({
                    ...prev,
                    cost: (props.cost * props.discount) + (ProductCase.toppings.length * 20)
                })
            } else if (ProductCase.size === 'big') {
                return ({
                    ...prev,
                    cost: Math.round((props.cost * props.discount * coeffs[props.type].up) - (coeffs[props.type].down * props.cost * props.discount)) + (ProductCase.toppings.length * 20)
                })
            }
        })
        //eslint-disable-next-line
    }, [ProductCase.toppings]);

    const showModal = () => {
        setVisible(true);
    };

    const error = () => {
        message.error('Ошибка в добавлении в корзину');
    };

    const handleOk = () => {
        setConfirmLoading(true);

        const newProductToCart = {
            ...ProductCase,
            serialId: uuidv4()
        };

        props.addToCartThunk(newProductToCart)
            .then(res => {
                setConfirmLoading(false);
                SetProductCase(initialState);
                setVisible(false);
            }).catch(err => {
            error();
            setConfirmLoading(false);
            SetProductCase(initialState);
            setVisible(false);
        })
    };

    const handleCancel = () => {
        SetProductCase(initialState);
        setVisible(false);
    };

    return (
        <>
            <div className='padding-class'>
                <Card
                    hoverable
                    className='ProductCard'
                    cover={<img alt={`${props.name}`} src={`/images/products/${props.type}/${props.id}/preview.jpeg`}/>}
                    onClick={showModal}
                >
                    <Card.Meta title={<ProductTitle discount={props.discount} cost={props.cost} name={props.name}/>}
                               description={props.shortdesk}/>
                </Card>
            </div>
            <Modal
                centered
                title={props.name}
                visible={visible}
                onOk={handleOk}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        Вернуться
                    </Button>,
                    <Button key="submit" type="primary" disabled={!props.isAuthenticated} loading={confirmLoading}
                            onClick={handleOk}>
                        Добавить в корзину
                    </Button>,
                ]}
            >
                <PizzaProduct ProductCase={ProductCase} SetProductCase={SetProductCase} cost={props.cost}
                              type={props.type} name={props.name} shortdesk={props.shortdesk} desk={props.desk}
                              id={props.id} discount={props.discount}/>
            </Modal>
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.user.isAuthenticated
    }
};

export default connect(mapStateToProps, {addToCartThunk})(Product);