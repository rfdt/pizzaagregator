import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router";
import './Products.css'
import Product from "../Product/Product";
import Loader from "../Loader/Loader";
import {connect} from 'react-redux';
import getProductTypeName from "../../Utils/getProductTypeName";



const Products = (props) => {

    const [filteredProducts, setFilteredProducts] = useState([]);
    const [productType, setProductType] = useState('pizza');

    useEffect(() => {
        setProductType(props.match.params.type);
    }, [props.match.params.type]);

    useEffect(()=> {
        const filtered = props.products.products.filter(product=>product.type === productType);
        setFilteredProducts(filtered);
        // eslint-disable-next-line
    },[productType]);

    return (
        <div className='products-container'>
            <div className='product-type-title'>
                {getProductTypeName(productType)}
            </div>
            {props.products.isLoading && <Loader/>}
            {filteredProducts.map((product)=>(
                <Product discount={product.discount}
                         cost={product.cost}
                         key={product._id}
                         type={product.type}
                         name={product.name}
                         shortdesk={product.shortdesk}
                         desk={product.desk}
                         id={product._id}
                />
            ))}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        products: state.products
    }
};
export default connect(mapStateToProps, null)(withRouter(Products));