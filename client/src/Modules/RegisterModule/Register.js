import React from 'react';
import * as Yup from "yup";
import {useFormik} from "formik";
import {Button, Form, Input, Alert} from "antd";
import {SmileOutlined} from "@ant-design/icons";
import { registerUserThunk} from "../../Store/User/userActions";
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";

const RegisterSchema = Yup.object().shape({
    name: Yup.string().required('Введите имя'),
    surName: Yup.string().required('Введите фамилию'),
    email: Yup.string().email('Введите почту').required('Обязательное поле'),
    password: Yup.string().required('Введите пароль').matches(
        "^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!#$%&? \"]).*$",
        "Пароль должен содержать 8 символов. \n Одно число и один специальный знак"
    ),
    mobilePhone: Yup.string().required('Введите телефон').matches(
        "^\\+?[78][-\\(]?\\d{3}\\)?-?\\d{3}-?\\d{2}-?\\d{2}$",
        "Номер должен иметь вид +7 978*******"
    ),
    adress: Yup.string().required('Введите адресс'),
    confirmPassword: Yup.string().required("Повторите пароль").oneOf([Yup.ref("password"), null], "Пароли не совпадают")
});

const Register = (props) => {

    const formItemLayout = {
        labelCol: {span: 6},
        wrapperCol: {span: 15},
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            surName: '',
            email: '',
            mobilePhone: '',
            password: '',
            confirmPassword: '',
            adress: ''
        },
        validationSchema: RegisterSchema,
        onSubmit: values => {
            const newUser = {
                name: values.name,
                surName: values.surName,
                mobilePhone: values.mobilePhone,
                email: values.email,
                password: values.password,
                adresses: [values.adress]
            };
            props.registerUserThunk(newUser);
        },
    });


    return (
        <div className="auth-form-container">
            <h1>Регистрация</h1>
            <form onSubmit={formik.handleSubmit} className='Login-form'>
                <Form.Item
                    {...formItemLayout}
                    label="Имя"
                    className='loginforminputs'
                    validateStatus={formik.errors.name && formik.touched.name && 'error'}
                    help={formik.touched.name && formik.errors.name}
                >
                    <Input placeholder="Введите имя" value={formik.values.name}
                           onChange={formik.handleChange}
                           name='name' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Фамилия"
                    className='loginforminputs'
                    validateStatus={formik.errors.surName && formik.touched.surName && 'error'}
                    help={formik.touched.surName && formik.errors.surName}
                >
                    <Input placeholder="Введите фамилию" value={formik.values.surName}
                           onChange={formik.handleChange}
                           name='surName' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Мобильный"
                    className='loginforminputs'
                    validateStatus={formik.errors.mobilePhone && formik.touched.mobilePhone && 'error'}
                    help={formik.touched.mobilePhone && formik.errors.mobilePhone}
                >
                    <Input placeholder="Введите мобильный телефон" value={formik.values.mobilePhone}
                           onChange={formik.handleChange}
                           name='mobilePhone' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Адрес"
                    className='loginforminputs'
                    validateStatus={formik.errors.adress && formik.touched.adress && 'error'}
                    help={formik.touched.adress && formik.errors.adress}
                >
                    <Input placeholder="Введите ваш адрес" value={formik.values.adress}
                           onChange={formik.handleChange}
                           name='adress' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label="Почта"
                    className='loginforminputs'
                    validateStatus={formik.errors.email && formik.touched.email && 'error'}
                    help={formik.touched.email && formik.errors.email}
                >
                    <Input placeholder="Введите почту" value={formik.values.email}
                           onChange={formik.handleChange}
                           name='email' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    className='loginforminputs'
                    label="Пароль"
                    validateStatus={formik.errors.password && formik.touched.password && 'error'}
                    help={formik.touched.password && formik.errors.password}
                >
                    <Input placeholder="Введите пароль" value={formik.values.password}
                           onChange={formik.handleChange}
                           type='password'
                           name='password' prefix={<SmileOutlined/>} onBlur={formik.handleBlur}/>
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    className='loginforminputs'
                    label="Подтвердите"
                    validateStatus={formik.errors.confirmPassword && formik.touched.confirmPassword && 'error'}
                    help={formik.touched.confirmPassword && formik.errors.confirmPassword}
                >
                    <Input placeholder="Повторите пароль" value={formik.values.confirmPassword}
                           onChange={formik.handleChange}
                           type='password'
                           name='confirmPassword' prefix={<SmileOutlined/>} onBlur={formik.confirmPassword}/>
                </Form.Item>
                <Form.Item>
                    <div className='submit-button-register'>
                        {
                            props.error.id === "REGISTER_FAIL" ?
                                props.error.msg.split("ID:")[0].trim() === 'Регистрация прошла успешно. Теперь вам нужно подвердить ваш номер телефона.' ?
                                <Alert
                                    message="Успешно"
                                    description={<div>{props.error.msg.split('ID:')[0]} <NavLink to={`/verify/${props.error.msg.split('ID:')[1]}`}>Нажмите сюда</NavLink></div>}
                                    type="success"
                                    showIcon
                                    closable
                                    style={
                                        {
                                            width: '100%',
                                            marginTop: '10px',
                                            marginBottom: '10px'
                                        }
                                    }
                               /> : <Alert
                                        message="Ошибка"
                                        description={props.error.msg}
                                        type="error"
                                        showIcon
                                        closable
                                        style={
                                            {
                                                width: '100%',
                                                marginTop: '10px',
                                                marginBottom: '10px'
                                            }
                                        }
                                    />
                           : null
                        }
                        <Button type="primary" htmlType="submit" style={{width: "180px", textAlign:'center'}} className='submit-button'>
                            Зарегистрироваться
                        </Button>
                        <NavLink to='/login'>Уже зарегистрированы? Авторизоваться.</NavLink>
                    </div>
                </Form.Item>
            </form>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        error: state.error
    }
};

export default connect(mapStateToProps, {registerUserThunk})(Register);