import React, {useState} from 'react';
import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router";
import {Alert, Button, Form, Input} from "antd";
import {activeUserThunk} from "../../Store/User/userActions";
import {SmileOutlined} from "@ant-design/icons";
import {formItemLayout} from "../CvModule/Cv";
import './VerifyModule.css';

const VerifyModule = (props) => {

    const [code, setCode] = useState('');

    if(!props.match.params.id) return <Redirect to={'/'}/>;

    const verify = () =>{
        if(!code) return 0;
        props.activeUserThunk(props.match.params.id, code);
    };


    return (
        <div className='auth-form-container'>
            <div className="verify__title">Верификация пользователя: {props.match.params.id}</div>

            <Form.Item
                {...formItemLayout}
                label="Введите код"
                className='loginforminputs'
            >
                <Input placeholder="Ваш код"
                       name='surName' prefix={<SmileOutlined/>} value={code} onChange={(e)=>{
                           setCode(e.target.value);
                }}/>
            </Form.Item>
            <Button type='primary' onClick={verify}>Активировать аккаунт</Button>
            {
                (props.error.id === "LOGIN_FAIL" || props.error.id === "AUTH_FAILED") &&
                <Alert
                    message="Ошибка"
                    description={props.error.msg}
                    type="error"
                    showIcon
                    style={
                        {
                            marginTop: '10px'
                        }
                    }
                />
            }
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        error: state.error
    }
};

export default connect(mapStateToProps, {activeUserThunk})(withRouter(VerifyModule));