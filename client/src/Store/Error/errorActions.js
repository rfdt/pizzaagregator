import {CLEAR_ERRORS, GET_ERRORS} from "./errorTypes";

export const returnErrors = (msg, status = 0 , id = 0) => {
    return {
        type: GET_ERRORS,
        payload: { msg, status, id }
    };
};

// CLEAR ERRORS
export const clearErrors = () => ({type: CLEAR_ERRORS});