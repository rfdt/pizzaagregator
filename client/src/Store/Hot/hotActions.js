import {clearErrors} from "../Error/errorActions";
import {tokenConfig} from "../User/userActions";

import {HOT_PRODUCTS_LOADED, HOT_PRODUCTS_LOADED_ERROR, HOT_PRODUCTS_LOADING} from "./hotTypes";
import { ProductsApi } from './../../Api/ProductsApi';

export const loadHotProductsThunk = () => (dispatch, getState) => {

    dispatch(hotProductsLoadingAC());
    dispatch(clearErrors());

    ProductsApi.loadHotProducts(tokenConfig(getState))
        .then(res =>
            dispatch(hotProductsLoadedAC(res.data))
        ).catch(err => {
        dispatch(hotProductsLoadedErrorAC());
    });
};


const hotProductsLoadingAC = () => ({
    type: HOT_PRODUCTS_LOADING
})

const hotProductsLoadedAC = (hotProducts) => ({
    type: HOT_PRODUCTS_LOADED, hotProducts: hotProducts
})

const hotProductsLoadedErrorAC = () =>({
    type: HOT_PRODUCTS_LOADED_ERROR
})