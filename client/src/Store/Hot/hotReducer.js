import {HOT_PRODUCTS_LOADED, HOT_PRODUCTS_LOADED_ERROR, HOT_PRODUCTS_LOADING} from "./hotTypes";

const initialState = {
    isLoading: false,
    isLoaded: false,
    hotProducts: [],
    status: null,
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case HOT_PRODUCTS_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case  HOT_PRODUCTS_LOADED:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                hotProducts: action.hotProducts,
                status: 'ok'
            };
        case HOT_PRODUCTS_LOADED_ERROR:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                hotProducts: [],
                status: 'error'
            };
        default:
            return state;
    }
}

export default productsReducer;