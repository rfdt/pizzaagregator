import {productNullCartAC, tokenConfig} from "../User/userActions";
import {ORDERS_ADD, ORDERS_LOADED, ORDERS_LOADING, ORDERS_RATED} from "./ordersTypes";
import {returnErrors} from "../Error/errorActions";
import { OrdersApi } from './../../Api/OrdersApi';

export const createOrder = (newOrder) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {

        OrdersApi.createOrder(newOrder, tokenConfig(getState))
            .then(res => {
                dispatch(ordersAddedAC(res.data.order));
                dispatch(productNullCartAC(res.data.updatedUserCart));
                resolve(res.data);
            }).catch(err => {
            returnErrors(err.data.msg, err.status);
            reject(err);
        });

    });

};

export const getOrders = () => (dispatch, getState) => {

    dispatch(ordersLoading());

    OrdersApi.getOrders(tokenConfig(getState))
        .then(res => {
            dispatch(ordersLoaded(res.data));
        })
};

export const rateOrders = (id, rate) => (dispatch, getState) => {

    OrdersApi.rateOrders(id, rate, tokenConfig(getState))
        .then(res => {
            dispatch(ordersRatedAC(res.data.id));
        });

};

export const activeCoupon = (couponname) => (dispatch, getState) => {
    return new Promise((resolve, reject) => {
        OrdersApi.activeCoupon(couponname, tokenConfig(getState))
            .then(res=>{
                resolve(res.data)
            })
            .catch(err=>{
                reject(err);
            })
    })
};

export const getSomeOrders = (stage) => (dispatch, getState) => {
    return OrdersApi.getSomeOrders(stage, tokenConfig(getState))
};

export const pickCookerOrder = (id, cookId, cookName) => (dispatch, getState) => {
    return OrdersApi.pickCookerOrder(id, cookId, cookName, tokenConfig(getState));
};

export const pickDelivOrder = (id, deliverymanId, deliverymanName, deliverymanPhone) => (dispatch, getState) => {
    return OrdersApi.pickDelivOrder(id, deliverymanId, deliverymanName, deliverymanPhone, tokenConfig(getState));
};

export const getCookerOrder = (id) => (dispatch, getState) => {
    return OrdersApi.getCookerOrder(id, tokenConfig(getState));
};

export const finishCook = (id) =>(dispatch, getState) => {
  return OrdersApi.finishCook(id, tokenConfig(getState));
};

export const finishDelivery = (id) =>(dispatch, getState) => {
  return OrdersApi.finishDelivery(id, tokenConfig(getState));
};


const ordersLoading = () =>({type: ORDERS_LOADING});

const ordersLoaded = (orders) =>({
    type: ORDERS_LOADED, orders: orders
});

const ordersAddedAC = (newOrder) => {
    return({type: ORDERS_ADD, newOrder: newOrder});
};

const ordersRatedAC = (id) =>{
    return({type: ORDERS_RATED, id:id});
};
