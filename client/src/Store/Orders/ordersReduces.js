import {ORDERS_ADD, ORDERS_LOADED, ORDERS_LOADING, ORDERS_RATED} from "./ordersTypes";


const initialState = {
    isLoading: false,
    orders: [],
}

const ordersReducer = (state = initialState, action) =>{
    switch (action.type){
        case ORDERS_LOADING:
            return{
                ...state,
                isLoading: true
            }
        case ORDERS_LOADED:
            return {
                ...state,
                isLoading: false,
                orders: action.orders
            }
        case ORDERS_ADD:
            return {
                ...state,
                orders: [...state.orders, action.newOrder]
            }
        case ORDERS_RATED:
            return {
                ...state,
                orders: state.orders.filter((order)=> order._id !== action.id)
            }
        default:
            return state;
    }
}

export default ordersReducer;