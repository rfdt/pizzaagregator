import {clearErrors} from "../Error/errorActions";
import {tokenConfig} from "../User/userActions";
import {PRODUCTS_LOADED, PRODUCTS_LOADED_ERROR, PRODUCTS_LOADING} from "./productsTypes";
import {ProductsApi} from './../../Api/ProductsApi';

export const loadProductsThunk = () => (dispatch, getState) => {

    dispatch(productsLoadingAC());
    dispatch(clearErrors());

    ProductsApi.loadProducts(tokenConfig(getState))
        .then(res =>
            dispatch(productsLoadedAC(res.data))
        ).catch(err => {
        dispatch(productsLoadedErrorAC());
    });
};

// export const addProduct = () => (dispatch, getState) =>{
//
// }

const productsLoadingAC = () => ({
    type: PRODUCTS_LOADING
})

const productsLoadedAC = (products) => ({
    type: PRODUCTS_LOADED, products: products
})

const productsLoadedErrorAC = () =>({
    type: PRODUCTS_LOADED_ERROR
})
