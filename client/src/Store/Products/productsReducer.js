import {PRODUCTS_LOADED, PRODUCTS_LOADED_ERROR, PRODUCTS_LOADING} from "./productsTypes";

const initialState = {
    isLoading: false,
    isLoaded: false,
    products: [],
    status: null,
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case  PRODUCTS_LOADED:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                products: action.products,
                status: 'ok'
            };
        case PRODUCTS_LOADED_ERROR:
            return {
                ...state,
                isLoading: false,
                isLoaded: true,
                products: [],
                status: 'error'
            };
        default:
            return state;
    }
}

export default productsReducer;