import {takeEvery, call, put, select} from "redux-saga/effects";
import {ASYNC_LOAD_USER, ASYNC_USER_CHECKOUT} from "../../User/userTypes";
import {authErrorAC, logincSuccesAc, loginFailAC, userLoadedAC, userLoadingAC} from "../../User/userActions";
import {clearErrors, returnErrors} from "../../Error/errorActions";
import {UserApi} from "../../../Api/UserApi";

export function tokenConfig(token) {

    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    if (token) {
        config.headers['pizza-agregator-token'] = token;
    }

    return config;
}

function* loadUserSaga() {

    yield put(userLoadingAC());
    yield put(clearErrors());
    try {
        const token = yield select(state => state.user.token);
        if (!token) throw new Error('Нет токена');
        const apiRes = yield call(() => UserApi.loadUser(tokenConfig(token)));
        yield put(userLoadedAC(apiRes.data));
    } catch (error) {
        yield put(authErrorAC());
    }
}

function* checkoutUserSaga({payload: {name, surName, email, newPassword}, resolve, reject}) {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    yield put(clearErrors());
    try {
        const body = JSON.stringify({name, surName, email, newPassword});
        const apiRes = yield call(() => UserApi.checkoutUser(body, config))
        yield put(logincSuccesAc({...apiRes.data}));
        resolve(apiRes)
    } catch (error) {
        yield put(returnErrors(error.response.data.msg, error.response.status, 'LOGIN_FAIL'));
        yield put(loginFailAC());
        reject(error)
    }

}

export function* userWatcher() {
    yield takeEvery(ASYNC_LOAD_USER, loadUserSaga);
    yield takeEvery(ASYNC_USER_CHECKOUT, checkoutUserSaga);
}