import {all} from 'redux-saga/effects';
import {userWatcher} from "./UserSagas/UserSagas";

export function* rootWatcher(){
    yield all([userWatcher()])
}