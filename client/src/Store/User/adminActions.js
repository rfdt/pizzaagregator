import {tokenConfig} from "./userActions";
import {UserApi} from "../../Api/UserApi";

export const addNewProduct = (newProduct) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.addNewProduct(newProduct, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getEveryDayPlane = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getEveryDayPlane(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getMonthPlane = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getMonthPlane(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getTodayOrders = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getTodayOrders(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getYestardayOrders = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getYestardayOrders(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getMonthOrders = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getMonthOrders(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const updateProduct = (product) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.updateProduct(product, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const updateProductPhoto = (product, currentPickedPhoto, photoFile) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.updateProductPhoto(product, currentPickedPhoto, photoFile, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const updateCarouselPhoto = (type, photoFile) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.updateCarouselPhoto(type, photoFile, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const viewCvs = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.viewCvs().then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const markedCvs = (id) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.markedCvs(id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const deleteCvs = (id) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.deleteCvs(id).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const getUsers = () => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.getAllUsers(tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const changeUserType = (userId, userType) => (dispatch, getState) => {

    return new Promise((resolve, reject) => {
        UserApi.changeUserType(userId, userType, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};

export const createCoupon = (coupon) => (dispatch, getState) => {
    return new Promise((resolve, reject) => {
        UserApi.createCoupon(coupon, tokenConfig(getState)).then(res => {
            resolve(res);
        }).catch(err => {
            console.log(err.response);
            reject(err);
        });

    })

};