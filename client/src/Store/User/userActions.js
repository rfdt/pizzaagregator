import {
    ADDING_PRODUCT, ASYNC_LOAD_USER, ASYNC_USER_CHECKOUT,
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    PRODUCT_ADDED_ERROR,
    PRODUCT_ADDED_SUCCESS, PRODUCT_NULL_CART,
    PRODUCT_REMOVED_ERROR,
    PRODUCT_REMOVED_SUCCESS,
    REGISTER_FAIL,
    REGISTER_SUCCESS, REMOVING_PRODUCT,
    USER_LOADED,
    USER_LOADING
} from "./userTypes";
import {clearErrors, returnErrors} from "../Error/errorActions";
import { UserApi } from './../../Api/UserApi';

export const tokenConfig = (getState) => {
    
    const token = getState().user.token;

    const config = {
        headers: {
            'Content-type': 'application/json'
        }
    };

    if (token) {
        config.headers['pizza-agregator-token'] = token;
    }

    return config;
};

export const loginUserThunk = ({email, password}) => (dispatch) => {
    
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
   
    const body = JSON.stringify({email, password});

    dispatch(clearErrors());

    return new Promise((resolve, reject) => {
        UserApi.loginUser(body, config)
            .then(res => {
                    dispatch(logincSuccesAc({...res.data}));
                    resolve(res.data);
                }
            ).catch(err => {
            dispatch(returnErrors(err.response.data.msg, err.response.status, 'LOGIN_FAIL'));
            dispatch(loginFailAC());
            reject(err);
        });
    });

};

export const activeUserThunk = (id, key) => (dispatch) => {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = JSON.stringify({id, key});

    dispatch(clearErrors());

    return new Promise((resolve, reject) => {
        UserApi.activeUser(body, config)
            .then(res => {
                    dispatch(logincSuccesAc({...res.data}));
                    resolve(res.data);
                }
            ).catch(err => {
            dispatch(returnErrors(err.response.data.msg, err.response.status, 'LOGIN_FAIL'));
            dispatch(loginFailAC());
            reject(err);
        });
    });

};

export const registerUserThunk = ({name, surName, mobilePhone, email, password, adresses}) => (dispatch) => {

    dispatch(clearErrors());
    
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    
    const body = JSON.stringify({name, surName, mobilePhone, email, password, adresses});

    UserApi.registerUser(body, config)
        .then(res =>
            dispatch(registerSuccesAc(res.data))
        ).catch(err => {
        dispatch(returnErrors(err.response.data.msg, err.response.status, 'REGISTER_FAIL'));
        dispatch(registerFailAC());
    });
};

export const addToCartThunk = (product) => (dispatch, getState) => {

    dispatch(addingProductAC());

    return new Promise((resolve, reject) => {
        UserApi.addToCart(product, tokenConfig(getState)).then(res => {
            dispatch(productAddedSuccessAC(res.data.cart));
            resolve(res);
        }).catch(err => {
            dispatch(productAddedErrorAC(err));
            reject(err);
        });

    })

};

export const romoveFromCartThunk = (editedCart) => (dispatch, getState) => {

    dispatch(removeProductAC());

    return new Promise((resolve, reject) => {
        UserApi.romoveFromCart(editedCart, tokenConfig(getState)).then(res => {
            dispatch(productRemovedSuccessAC(res.data.cart));
            resolve(res);
        }).catch(err => {
            dispatch(productRemovedErrorAC(err));
            reject(err);
        });

    })

};

export const addCv = ({name, surName, mobilePhone, linkToPhoto, cvVacansies, cvText, age}) => (dispatch, getState) => {

    const body = JSON.stringify({name, surName, mobilePhone, linkToPhoto, cvVacansies, cvText, age});

    return new Promise((resolve, reject) => {
        UserApi.addCv(body).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });

    })

};


export const logout = () => ({type: LOGOUT_SUCCESS});

const registerFailAC = () => ({type: REGISTER_FAIL});
export const loginFailAC = () => ({type: LOGIN_FAIL});

const registerSuccesAc = (data) => ({
    type: REGISTER_SUCCESS,
    data: data
});

export const logincSuccesAc = (data) => ({
    type: LOGIN_SUCCESS,
    data: data
});


export const userLoadingAC = () => ({
    type: USER_LOADING
});

export const userLoadedAC = (data) => ({
    type: USER_LOADED,
    data: data
});

export const authErrorAC = () => ({type: AUTH_ERROR});

const addingProductAC = () => ({type: ADDING_PRODUCT});

const removeProductAC = () => ({type: REMOVING_PRODUCT});

const productAddedSuccessAC = (cart) => ({type: PRODUCT_ADDED_SUCCESS, cart: cart});

const productRemovedSuccessAC = (cart) => ({type: PRODUCT_REMOVED_SUCCESS, cart: cart});

const productAddedErrorAC = (error) => ({type: PRODUCT_ADDED_ERROR, error: error});

const productRemovedErrorAC = (error) => ({type: PRODUCT_REMOVED_ERROR, error: error});

export const productNullCartAC = (cart) =>{
   return ({type: PRODUCT_NULL_CART, cart: cart});
};

export const asyncLoadUserAC = () => ({type: ASYNC_LOAD_USER});

export const asyncUserCheckout = ({name, surName, email, newPassword}, resolve, reject) =>({
    type: ASYNC_USER_CHECKOUT, payload: {name, surName, email, newPassword}, resolve, reject
});