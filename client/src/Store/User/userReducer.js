import {
    AUTH_ERROR,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS, PRODUCT_ADDED_SUCCESS, PRODUCT_NULL_CART, PRODUCT_REMOVED_SUCCESS, REGISTER_FAIL,
    REGISTER_SUCCESS,
    USER_LOADED,
    USER_LOADING
} from "./userTypes";


const initialState = {
    token: localStorage.getItem('pizza-agregator-token'),
    isAuthenticated: false,
    isLoading: false,
    isLoaded: false,
    user: {
        name: null,
        surname: null,
        mobilePhone: null,
        userType: null,
        adresses: [],
        cart: []
    }
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                isLoading: true,
                isLoaded: false
            };
        case USER_LOADED:
            return {
                ...state,
                user: {...action.data},
                isAuthenticated: true,
                isLoading: false,
                isLoaded: true
            };
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('pizza-agregator-token', action.data.token);
            return {
                ...state,
                ...action.data,
                isAuthenticated: true,
                isLoading: false,
                isLoaded: true
            };
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
            localStorage.removeItem('pizza-agregator-token');
            return {
                ...state,
                token: null,
                isAuthenticated: false,
                isLoading: false,
                isLoaded: true,
                user: {
                    name: null,
                    surname: null,
                    mobilePhone: null,
                    userType: null,
                    adresses: [],
                    cart: []
                }
            };
        case PRODUCT_ADDED_SUCCESS:
        case PRODUCT_REMOVED_SUCCESS:
        case PRODUCT_NULL_CART:
            return {
                ...state,
                user: {
                    ...state.user,
                    cart: action.cart
                }
            }
        default:
            return state;
    }
};

export default userReducer;
