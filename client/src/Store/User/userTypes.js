export const ITEMS_LOADING = 'ITEMS_LOADING';
export const USER_LOADING = "USER_LOADING";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const ADDING_PRODUCT = "ADDING_PRODUCT";
export const PRODUCT_ADDED_SUCCESS = "PRODUCT_ADDED_SUCCESS";
export const PRODUCT_ADDED_ERROR = "PRODUCT_ADDED_ERROR";
export const REMOVING_PRODUCT = "REMOVING_PRODUCT";
export const PRODUCT_REMOVED_SUCCESS = "PRODUCT_REMOVED_SUCCESS";
export const PRODUCT_REMOVED_ERROR = "PRODUCT_REMOVED_ERROR";
export const PRODUCT_NULL_CART = 'PRODUCT_NULL_CART';

export const ASYNC_LOAD_USER = "LOAD_USER";
export const ASYNC_USER_CHECKOUT = 'ASYNC_USER_CHECKOUT';