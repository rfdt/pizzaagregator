import {applyMiddleware, combineReducers, createStore} from "redux";
import createSagaMiddleware from 'redux-saga'
import {composeWithDevTools} from "redux-devtools-extension";
import {rootWatcher} from "./Sagas/indexSaga";
import thunk from "redux-thunk";

import userReducer from "./User/userReducer";
import productsReducer from './Products/productsReducer'
import errorReducer from "./Error/errorReducer";
import hotProductsReducer from './Hot/hotReducer';
import ordersReducer from "./Orders/ordersReduces";


const reducers = combineReducers({
    user: userReducer,
    products: productsReducer,
    hotProducts: hotProductsReducer,
    orders: ordersReducer,
    error: errorReducer,
});

const sagaMiddleware = createSagaMiddleware();

const storeMiddlewares = [thunk, sagaMiddleware];

let store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(...storeMiddlewares)
    )
);

sagaMiddleware.run(rootWatcher);

export default store;
