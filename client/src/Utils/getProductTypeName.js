const getProductTypeName = (type) =>{
    if (type === 'pizza') {
        return 'Пицца'
    }else if (type === 'rolls') {
        return 'Роллы'
    }else if (type === 'frrolls') {
        return 'Запеченные роллы'
    }else if (type === 'temprolls') {
        return 'Темпура роллы'
    }else if (type === 'wok') {
        return 'WOK Лапша'
    }else if (type === 'desserts') {
        return 'Дессерты'
    }else if (type === 'drinks') {
        return 'Напитки'
    }else if (type === 'sets') {
        return 'Наборы'
    }else{
        return 'Незнакомая нам категория'
    }
};

export default getProductTypeName;