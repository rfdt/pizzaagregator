export const coeffs = {
    rolls:{
        up: 1.4,
        down: 0.2
    },
    frrolls:{
        up: 1.4,
        down: 0.2
    },
    temprolls:{
        up: 1.4,
        down: 0.2
    },
    drinks:{
        up: 2.7,
        down: 0.85
    },
    pizza:{
        up: 2.2,
        down: 0.35
    },
    wok:{
        up: 2,
        down: 0.3
    },
    desserts: {
        up: 2.2,
        down: 0.25
    },
};