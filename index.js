const express = require('express');
const path = require('path');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routes/auth');
const userRouter = require('./routes/user');
const productsRouter = require('./routes/products');
const ordersRouter = require('./routes/orders');
const couponRouter = require('./routes/coupon');
const cvRouter = require('./routes/cv');
const adminRouter = require('./routes/admin');
    
app.use(express.static(path.join(__dirname, 'client', 'build')));
app.use(cors());
app.use(cookieParser());
app.use(fileUpload({
    createParentPath: true
}));
app.use(express.json());
if(process.env.MODE === 'dev' || process.env.MODE === 'production') app.use(logger('dev'));

mongoose.connect(process.env.DB, //process.env.db - бдд в .env
    {
        useNewUrlParser:true,
        useCreateIndex: true,
        useUnifiedTopology:true,
        useFindAndModify: false,
    })
    .then(()=> console.log('MongoDb connected', process.env.DB))
    .catch((err)=> console.log('MongoDb ERROR '+err));

app.use('/api/auth', authRouter);
app.use('/api/user', userRouter);
app.use('/api/products', productsRouter);
app.use('/api/orders', ordersRouter);
app.use('/api/coupon', couponRouter);
app.use('/api/cv', cvRouter);
app.use('/api/admin', adminRouter);

if(process.env.mode === 'production') {
    app.use((req, res, next) => {
        return res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
    });
}

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => console.log(`Sever listen on port ${process.env.PORT} in ${process.env.MODE} mode`));