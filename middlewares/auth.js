var jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET;

const auth = (req, res, next) => {
    const token = req.header('pizza-agregator-token');

    if (!token) {
        return res.status(401).json({msg: "Нет токена, плохой запрос"});
    }

    try {

        const decoded = jwt.verify(token, jwtSecret);

        req.user = decoded;

        next();
    } catch (error) {
        res.status(400).json({msg: "Пользователь не валиден. Перезайдите"});
    }
};

module.exports = auth;
