const express = require('express');
const moment = require('moment');
const router = express.Router();
const auth = require('../middlewares/auth');
const Order = require('../schemas/Order');
const Admin = require('../schemas/AdminSettings');
const User = require('../schemas/User');

router.get('/gettodayorders', async (req, res)=>{
try {

    const startDay = moment().startOf('day').toISOString();
    const endDay = moment().endOf('day').toISOString();

    const todayOrder = await Order.find({
        createdAt: {
            '$gte': startDay, '$lte': endDay
        }
    });

    res.status(200).send({
        todayOrder
    })
} catch (error) {
    res.status(400).json({msg: error.message})
}
});

router.get('/getyestardayorders', async (req, res)=>{
    try {
    
        const startDay = moment().subtract(1, 'days').startOf('day').toString();
        const endDay = moment().subtract(1, 'days').endOf('day').toString();
    
        const yestardayOrder = await Order.find({
            createdAt: {
                '$gte': startDay, '$lte': endDay
            }
        });
    
        res.status(200).send({
            yestardayOrder
        })
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.get('/getmonthorders', async (req, res)=>{
    try {
    
        const startDay = moment().startOf('month').toString();
        const endDay = moment().endOf('month').toString();
    
        const monthOrders = await Order.find({
            createdAt: {
                '$gte': startDay, '$lte': endDay
            }
        });
    
        res.status(200).send({
            monthOrders
        })
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.post('/createdayplane', async (req, res)=>{
    try {

        const newDatePlane = new Admin({
            parametres: 'dayPlane',
            value: req.body.value
        })

        const savedPlane = await newDatePlane.save();

        res.status(200).send({
           ...savedPlane
        })

    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.post('/createmonthplane', async (req, res)=>{
    try {

        const newDatePlane = new Admin({
            parametres: 'monthPlane',
            value: req.body.value
        })

        const savedPlane = await newDatePlane.save();

        res.status(200).send({
           ...savedPlane
        })

    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.get('/getdayplane', async (req, res)=>{
    try {
        
        const dayPlane = await Admin.find({parametres: 'dayPlane'})
    
        res.status(200).send({
            dayPlane: dayPlane[dayPlane.length - 1] 
        })
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.get('/getmonthplane', async (req, res)=>{
    try {
        
        const monthPlane = await Admin.find({parametres: 'monthPlane'})
    
        res.status(200).send({
            monthPlane: monthPlane[monthPlane.length - 1]
        })
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.get('/getallusers', auth, async (req, res)=>{
    try {
		
        const userId = req.user.id;
        const admin = await User.findById(userId);
        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к добавлению товара');
		
        const users = await User.find({});
    
        res.status(200).send(users)
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.get('/changeusertype/:userid/:usertype', auth, async (req, res)=>{
    try {
        const id = req.params.userid;
        const type = req.params.usertype;
        const userId = req.user.id;
        const admin = await User.findById(userId);
        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к добавлению товара');

        const userChanged = await User.findByIdAndUpdate(id, {$set: {
            userType: type
        }}, {new: true});
        if (!userChanged) throw  Error('Проблема с обновлением');

        res.status(200).send(userChanged)
    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.post('/updatehotphoto/:type', auth, async (req, res)=>{
    try {
        const photoType = req.params.type;
        const userId = req.user.id;

        const admin = await User.findById(userId);

        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к обновлению товара');

        if (!req.files) throw Error('Нет файла для загрузки');

        const photoFile = req.files.photo;

        photoFile.mv(`${__dirname}/../client/build/images/hot/carousel/car_${photoType}.jpg`, function (err) {
            if (err)  throw Error('Ошибка загрузки');
            return res.status(200).send({name: `car_${photoType}.jpg`, path: `/car_${photoFile.name}.jpg`});
        });

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

module.exports = router;