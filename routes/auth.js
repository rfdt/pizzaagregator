var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var auth = require('../middlewares/auth');
var User = require('../schemas/User');
var RandomKey = require('../ServerUtils/RandomKey');
var twilio = require('twilio');

const jwtSecret = process.env.JWT_SECRET;
const twilioSid = process.env.TWILIO_SID;
const twilioToken = process.env.TWILIO_TOKEN;
const twilioFrom = process.env.TWILIO_FROM;

var client = new twilio(twilioSid, twilioToken);

/* Авторизация  */
router.post('/login', async (req, res) => {
    const {email, password} = req.body;

    if (!email || !password ) {
        return res.status(400).json({msg: "Введите все поля"});
    }

    try {
        const user = await User.findOne({email});
        if (!user) throw Error('Пользователь не существует');

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) throw Error('Неправильный пароль');

        if(!user.confirmed) throw Error(`Вы не подтвердили номер телефона. ID:${user._id}`);

        const token = jwt.sign({id: user._id}, jwtSecret, {expiresIn: '720h'});
        if (!token) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');

        res.status(200).json({
            token,
            user: {
                id: user._id,
                name: user.name,
                userType: user.userType,
                surName: user.surName,
                mobilePhone: user.mobilePhone,
                email: user.email,
                adresses: user.adresses,
                cart: user.cart,
                register_date: user.register_date,
            }
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
});

/* Регистрация */
router.post('/register', async (req, res) => {
    const {
        name,
        surName,
        mobilePhone,
        email,
        adresses,
        cart,
        password
    } = req.body;

    if (!name || !email || !password) {
        return res.status(400).json({msg: 'Введите все поля'});
    }

    try {
        const user = await User.findOne({email});
        if (user) throw Error('Пользователь с этим адресом электронной почты уже существует');

        const number = await User.findOne({mobilePhone});
        if (number) throw Error('Пользователь с таким номером уже существует');

        const salt = await bcrypt.genSalt(10);
        if (!salt) throw Error('Что-то пошло не так с bcrypt. Пожалуйста подождите и повторите позже');

        const hash = await bcrypt.hash(password, salt);
        if (!hash) throw Error('Что-то пошло не так с хешированием пароля. Пожалуйста подождите и повторите позже');

        const codeConfirm = RandomKey(10000, 99999);
        if(!codeConfirm) throw Error('Что-то пошло не так с хешированием ключа. Пожалуйста подождите и повторите позже');

        const newUser = new User({
            name,
            surName,
            mobilePhone,
            email,
            adresses,
            cart,
            codeConfirm,
            password: hash,

        });

        const savedUser = await newUser.save();
        if (!savedUser) throw Error('Что-то пошло не так, при сохранении вашего аккаунта. Пожалуйста подождите и повторите позже');

        const message = await client.messages.create({
            body: `HOLA Ваш код: ${savedUser.codeConfirm}`,
            to: savedUser.mobilePhone,
            from: twilioFrom
        });


        // ОЧЕНЬ ЖЕСТКИЙ АНТИПАТТЕРН REST-API
        if (!savedUser.confirmed) throw Error(`Регистрация прошла успешно. Теперь вам нужно подвердить ваш номер телефона. ID:${savedUser._id}`);

        const token = jwt.sign({id: savedUser._id}, jwtSecret, {
            expiresIn: '720h'
        });
        if (!token) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');

        res.status(200).json({
            token,
            user: {
                id: savedUser._id,
                name: savedUser.name,
                surName: savedUser.surName,
                mobilePhone: savedUser.mobilePhone,
                email: savedUser.email,
                adresses: savedUser.adresses,
                cart: savedUser.cart,
                userType: savedUser.userType,
                register_date: savedUser.register_date,
            }
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
});

//Верификация
router.post('/verify', async (req, res) => {
    const {
       id, key
    } = req.body;

    if (!id || !key) {
        return res.status(400).json({msg: 'Введите все поля'});
    }

    try {
        const findedUser = await User.findById(id);
        if(!findedUser) throw Error('Пользователь не существует');
        if(findedUser.confirmed) throw Error('Пользователь уже подтвержден');
        if(key !== findedUser.codeConfirm) throw Error('Ключ неверный');

        const savedUser = await User.findByIdAndUpdate(id, {$set:{confirmed:true}}, {new: true});

        const token = jwt.sign({id: savedUser._id}, jwtSecret, {
            expiresIn: '720h'
        });
        if (!token) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');

        res.status(200).json({
            token,
            user: {
                id: savedUser._id,
                name: savedUser.name,
                surName: savedUser.surName,
                mobilePhone: savedUser.mobilePhone,
                email: savedUser.email,
                adresses: savedUser.adresses,
                cart: savedUser.cart,
                userType: savedUser.userType,
                register_date: savedUser.register_date,
            }
        });
    } catch (error) {
        res.status(400).json({msg: error.message});
    }
});

//Восстановление пароля
router.post('/checkout', async (req, res) => {
    try {
        const {name, surName, email, newPassword} = req.body;

        const salt = await bcrypt.genSalt(10);
        if (!salt) throw Error('Что-то пошло не так с bcrypt. Пожалуйста подождите и повторите позже');

        const hash = await bcrypt.hash(newPassword, salt);
        if (!hash) throw Error('Что-то пошло не так с хешированием пароля. Пожалуйста подождите и повторите позже');

        const user = await User.findOneAndUpdate({name, surName, email},{$set:{password:hash}}, {new: true});
        if (!user) throw Error('Пользователь не существует!');
        if (!user.confirmed) throw Error('Пароль восстановлен, но аккаунт не автиврован!');
        
        const token = jwt.sign({id: user._id}, jwtSecret, {
            expiresIn: '720h'
        });
        if (!token) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');  

        res.status(200).json({ 
            token,
            user:{
            id: user._id,
            userType: user.userType,
            name: user.name,
            surName: user.surName,
            mobilePhone: user.mobilePhone,
            email: user.email,
            adresses: user.adresses,
            cart: user.cart,
            register_date: user.register_date,
        }});

    } catch (error) {
        res.status(400).json({ msg: error.message });
    }
});

/* Получения пользователя. */
router.get('/user', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        if (!user) throw Error('User does not exist');
        res.status(200).json( {
            id: user._id,
            userType: user.userType,
            name: user.name,
            surName: user.surName,
            mobilePhone: user.mobilePhone,
            email: user.email,
            adresses: user.adresses,
            cart: user.cart,
            register_date: user.register_date,
        });

    } catch (error) {
        res.status(400).json({ msg: error.message });
    }
});

module.exports = router;
