const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');
const User = require('../schemas/User');
const Coupon = require('../schemas/Coupon');

router.get('/:couponname', auth, async (req,res)=>{
    try {

        const couponname = req.params.couponname;

        if(!couponname) throw Error('Извините. Проблема с запросом!');

        const findedCoupon = await Coupon.find({name: couponname});

        if(!findedCoupon) throw Error('Извините. Но нет такого промокода!');

        res.status(200).send(findedCoupon[0]);

    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

router.post('/create', auth, async (req, res)=>{
    try {

        const userId = req.user.id;
        const admin = await User.findById(userId);

        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к добавлению товара');

        const newCoupone = new Coupon({
            type: req.body.type,
            minsum: req.body.minsum,
            name: req.body.name,
            perc: req.body.perc,
            sum: req.body.sum,
            gift: req.body.gift
        });

        const savedNewCoupon = await newCoupone.save();

        if (!savedNewCoupon) throw Error('Проблема с сохранением купона');

        res.status(200).send(savedNewCoupon);

    } catch (error) {
        res.status(400).json({msg: error.message})
    }
});

module.exports = router;