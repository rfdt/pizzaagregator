var express = require('express');
var router = express.Router();
var Cv = require('../schemas/Cv');

router.get('/', async (req,res)=>{
    try {

        const Cvs = await Cv.find({});
        if(!Cvs) throw Error('Проблема с поиском');

        res.status(200).send(Cvs);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.post('/create', async (req,res)=>{
    try {
        const {name, surName, age, mobilePhone, cvText, linkToPhoto, cvVacansies} = req.body;

        const newCv = new Cv({
            name,
            surName,
            age,
            mobilePhone,
            cvText,
            linkToPhoto,
            cvVacansies
        });

        const newCvSaved = await newCv.save();

        if(!newCvSaved) throw Error('Проблема с сохранением заявки');

        res.status(200).send('Created');

    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.get('/marked/:id', async (req,res)=>{
    const id = req.params.id;
    try {
        const Cvs = await Cv.findByIdAndUpdate(id, { $set: {marked: 'true'}}, {new: true});
        if(!Cvs) throw Error('Проблема с поиском');

        res.status(200).send(Cvs);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.get('/delete/:id', async (req,res)=>{
    const id = req.params.id;
    try {
        const Cvs = await Cv.findOneAndRemove({_id: id})
        if(!Cvs) throw Error('Проблема с поиском');

        res.status(200).send(Cvs);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

module.exports = router;