var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');
var Order = require('../schemas/Order');
var User = require('../schemas/User');


router.get('/', auth, async (req, res) => {
    try {

        const customerId = req.user.id

        const Orders = await Order.find({ customerId: customerId, stage: { $in: [0, 1, 2, 3, 4] } });

        if (!Orders) throw Error('Проблема с поиском заказов!');

        res.status(200).send(Orders);

    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.get('/:stage', auth, async (req, res) => {
    try {

        const stage = req.params.stage;

        const Orders = await Order.find({ stage: parseInt(stage, 10) });

        if (!Orders) throw Error('Проблема с поиском заказов!');

        res.status(200).send(Orders);

    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.post('/create', auth, async (req, res) => {
    try {

        const {
            customerId,
            customerName,
            customerSurName,
            customerPhone,
            customerAdress,
            cart,
            isUsedCupon,
            userPreferences,
            totalCost,
            payMethod
        } = req.body

        if (!customerId || !customerName || !customerSurName || !customerPhone || !customerAdress || !cart || !totalCost || !payMethod) throw Error('Нет данных');

        const newOrder = new Order({
            customerId,
            customerName,
            customerSurName,
            customerPhone,
            customerAdress,
            cart,
            isUsedCupon,
            userPreferences,
            totalCost,
            payMethod
        })

        if (!newOrder) throw Error('Проблема с созданием заказа');

        const newOrderSaved = await newOrder.save();

        if (!newOrderSaved) throw Error('Проблема с сохранением заказа');

        const updatedUserCart = await User.findByIdAndUpdate(customerId, { cart: [] }, { new: true });

        if (!updatedUserCart) throw Error('Проблема с обновлением вашей корзины');

        res.status(200).send({
            order: newOrderSaved,
            updatedUserCart: updatedUserCart.cart
        });

    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

router.post('/rate', auth, async (req, res) => {
    try {
        const { id, rate } = req.body;

        if (!id || !rate) throw Error('Проблема с данными');

        const findAndUpdated = await Order.findByIdAndUpdate(id, { rate: rate, stage: 5 }, { new: true });

        res.status(200).send({ id: findAndUpdated._id });

    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

router.post('/pickcookerorder', auth, async (req, res) => {
    try {

        const { id, cookId, cookName } = req.body;

        if (!id || !cookId || !cookName) throw Error('Проблема с данными');

        const findAndUpdated = await Order.findByIdAndUpdate(id, { stage: 1, cookId: cookId, cookName }, { new: true });

        res.status(200).send({ id: findAndUpdated._id });

    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

router.post('/pickdelivorder', auth, async (req, res) => {
    try {

        const { id, deliverymanId, deliverymanName, deliverymanPhone } = req.body;

        if (!id || !deliverymanId || !deliverymanName || !deliverymanPhone) throw Error('Проблема с данными');

        const findAndUpdated = await Order.findByIdAndUpdate(id, { stage: 3, deliverymanId, deliverymanName, deliverymanPhone }, { new: true });

        res.status(200).send({ id: findAndUpdated._id });

    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

router.get('/getcookerorder/:id', auth, async (req, res) => {
    try {

        const { id } = req.params;

        if (!id) throw Error('Проблема с получением id');

        const findOrder = await Order.findById(id);

        res.status(200).send(findOrder);

    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
});

router.post('/finishcookorder', auth, async (req, res)=>{
    try {
        const id = req.body.id;

        const findAndUpdated = await Order.findByIdAndUpdate(id, { stage: 2, }, { new: true });

        res.status(200).send(findAndUpdated);
    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

router.post('/finishdeliveryorder', auth, async (req, res)=>{
    try {
        const id = req.body.id;

        const findAndUpdated = await Order.findByIdAndUpdate(id, { stage: 4, }, { new: true });

        res.status(200).send(findAndUpdated);
    } catch (error) {
        res.status(400).json({ msg: error.message })
    }
})

module.exports = router;