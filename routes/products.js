var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');
var Product = require('../schemas/Product');
var User = require('../schemas/User');

router.get('/', async (req, res)=>{
    try{

        const Products = await Product.find({});
        if (!Products) throw Error ('Что-то не так с поиском продуктов');

        res.status(200).send(Products);

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

router.get('/hot', async (req, res)=>{
    try{

        const Hot = await Product.find({hot: true});
        if (!Hot) throw Error ('Что-то не так с поиском продуктов');

        res.status(200).send(Hot);

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

router.post('/create', auth, async (req, res)=>{
    try {
        const userId = req.user.id;
        const admin = await User.findById(userId);
        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к добавлению товара');

        const {type, name, desk, shortdesk, cost, discount, hot} = req.body;

        const newProduct = new Product({
            type,
            name,
            desk,
            shortdesk,
            cost,
            discount,
            hot
        });

        if (!newProduct) throw Error('Проблема с добавлением товара!');

        const newProductSaved = await newProduct.save();

        if(!newProductSaved) throw Error('Проблема с сохранением');

        res.status(200).send(newProductSaved);

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

router.post('/update/:id', auth, async (req, res)=>{
    try {
        const productId = req.params.id;
        const userId = req.user.id;
        const admin = await User.findById(userId);

        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к обновлению товара');

        const updateProduct = req.body;
        
        const updatedProduct = await Product.findByIdAndUpdate(productId, updateProduct, {new: true})
        if (!updatedProduct) throw Error('Проблема с обновлением товара!');

        res.status(200).send(updatedProduct);

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

router.post('/updatephoto/:type/:id/:phototype', auth, async (req, res)=>{
	try {
		const productType = req.params.type;
        const productId = req.params.id;
        const photoType = req.params.phototype;
		const userId = req.user.id;
		
        const admin = await User.findById(userId);

        if (!admin) throw Error('Проблема с определением администратора');
        if (admin.userType !== 'admin') throw Error('Вы не имеете доступа к обновлению товара');

		if (!req.files) throw Error('Нет файла для загрузки');

		const photoFile = req.files.photo;

		photoFile.mv(`${__dirname}/../client/build/images/products/${productType}/${productId}/${photoType}.jpeg`, function (err) {
			if (err)  throw Error('Ошибка загрузки');
			return res.status(200).send({name: `${photoType}.jpeg`, path: `/${photoFile.name}`});
		});

    }catch(error){
        res.status(400).json({msg: error.message});
    }
});

module.exports = router;

