var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');
const User = require('../schemas/User');

router.post('/addtocart', auth, async (req, res) => {
    try {
        const id = req.user.id;
        if (!id) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');

        const newProduct = {
            serialId: req.body.serialId,
            cost: req.body.cost,
            discount: req.body.discount,
            id: req.body.id,
            name: req.body.name,
            size: req.body.size,
            toppings: req.body.toppings,
            type: req.body.type,
            userPreferences: req.body.userPreferences
        }

        if (!newProduct) throw Error('Что-то не так с добавлением. Перезайдите и попробуйте позже');

        const updatedCart = await User.findByIdAndUpdate(id, {
            $push: {cart: newProduct}
        }, {new: true});

        if (!updatedCart) throw Error('Что-то не так с сохранением. Попробуйте позже')

        res.status(200).json({
            cart: updatedCart.cart
        })

    } catch (err) {
        res.status(400).json({msg: err.message});
    }
})

router.post('/removefromcart', auth, async (req, res) => {
    try {
        const id = req.user.id;
        if (!id) throw Error('Что-то не так c ключом. Перезайдите и попробуйте позже');

        const editedCart = req.body
        if (!editedCart) throw Error('Что-то не так с добавлением. Перезайдите и попробуйте позже');

        const updatedCart = await User.findByIdAndUpdate(id, {
            $set: {cart: editedCart}
        }, {new: true});
        if (!updatedCart) throw Error('Что-то не так с сохранением. Попробуйте позже')

        res.status(200).json({
            cart: updatedCart.cart
        })

    } catch (err) {
        res.status(400).json({msg: err.message});
    }
})

module.exports = router;