const {Schema, model} = require('mongoose');

const AdminSchema = new Schema({
   parametres: {
       type: String,
       required: true
   },
   value:{
       type: String,
       required: true
   }
});

const Admin = model('AdminSchema', AdminSchema);

module.exports = Admin;