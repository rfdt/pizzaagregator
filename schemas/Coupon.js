const Mongoose = require('mongoose');

const CouponSchema = new Mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    minsum: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    perc: {
        type: Number,
        default: 0
    },
    sum: {
        type: Number,
        default: 0
    },
    gift: {
        type: {},
        default: {}
    },
});

const Coupon = Mongoose.model('Coupon', CouponSchema);

module.exports = Coupon;