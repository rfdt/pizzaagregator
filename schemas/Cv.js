const {Schema, model} = require('mongoose');

const CvSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    surName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    mobilePhone: {
        type: String,
        required: true,
    },
    cvText: {
        type: String,
        required: true
    },
    linkToPhoto:{
        type: String,
    },
    cvVacansies: {
        type: String,
        required: true
    },
    marked: {
        type: Boolean,
        default: false
    }
});

const Cv = model('Cv', CvSchema);

module.exports = Cv;