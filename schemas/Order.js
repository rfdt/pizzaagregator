const {Schema, model} = require('mongoose');

const OrderSchema = new Schema({
    customerId: {
        type: String,
        required: true
    },
    customerName:{
        type: String,
        required: true
    },
    customerSurName:{
        type: String,
        required: true
    },
    customerPhone:{
        type: String,
        required: true
    },
    customerAdress:{
        type: String,
        required: true
    },
    cart:{
        type: [{}],
        required: true
    },
    totalCost: Number,
    payMethod: String,
    stage:{
        type: Number,
        default: 0
    },
    isUsedCupon:{
        type: Boolean,
        default:false
    },
    cookId: String,
    cookName: String,
    deliverymanId: String,
    deliverymanName: String,
    deliverymanPhone: String,
    userPreferences: {
        type: String,
        default: ''
    },
    rate: Number,
    createdAt:{
        type: Date,
        default: Date.now()
    }
});

const Order = model('Order', OrderSchema);

module.exports = Order;