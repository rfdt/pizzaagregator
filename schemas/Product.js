const {Schema, model} = require('mongoose');

const ProductSchema = new Schema({
    type: {
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    desk:{
        type: String,
        required: true,
    },
    shortdesk:{
        type: String,
        required: true,
    },
    cost: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: true,
        default: 1
    },
    hot: {
        type: Boolean,
        default: false
    }
});

const Product = model('Product', ProductSchema);

module.exports = Product;

