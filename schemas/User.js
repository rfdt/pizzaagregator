const {Schema, model} = require('mongoose');

const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    surName: {
        type: String,
        required: true
    },
    mobilePhone: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    adresses: {
        type: [String],
        default: []
    },
    cart: {
        type: [{}],
        default: [],
    },
    register_date: {
        type: Date,
        default: Date.now
    },
    userType: {
        type: String,
        default: 'user'
    },
    confirmed: {
        type: Boolean,
        default: false
    },
    codeConfirm: {
        type: String,
        required: true
    }
});

const User = model('User', UserSchema);

module.exports = User;
